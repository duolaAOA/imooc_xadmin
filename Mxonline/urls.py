# -*- coding:utf-8 -*-
"""Mxonline URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import xadmin
from django.conf.urls import url, include
from django.views.static import serve

from Mxonline.settings import MEDIA_ROOT, STATIC_ROOT
from apps.users.views import LoginView, RegisterView, ActiveUserView, ForgetPwdView
from apps.users.views import ResetView, ModifyPwdView, LogoutView, IndexView

urlpatterns = [

    url(r'^xadmin/', xadmin.site.urls),
    url('^$',IndexView.as_view(),name='index'),
    url('^login/$',LoginView.as_view(),name="login"),
    url('^logout/$',LogoutView.as_view(),name="logout"),
    url('^register/$', RegisterView.as_view(), name="register"),
    url('^captcha/', include('captcha.urls')),
    url('^active/(?P<active_code>.*)/$',ActiveUserView.as_view(),name="user_active"),
    url('^forget/$',ForgetPwdView.as_view(),name="forget_pwd"),
    url('^reset/(?P<active_code>.*)/$',ResetView.as_view(),name="reset_pwd"),
    url('^modify_pwd/$',ModifyPwdView.as_view(),name="modify_pwd"),

    #课程机构url配置
    url('^org/', include('apps.organization.urls', namespace='org')),

    #课程相关url配置
    url('^course/', include('apps.courses.urls', namespace='course')),

    #配置上传文件的访问处理函数
    url('^media/(?P<path>.*)$', serve,{"document_root":MEDIA_ROOT}),

    #自定义代理服务
    url('^static/(?P<path>.*)$', serve,{"document_root":STATIC_ROOT}),

    # 课程相关url配置
    url('^users/', include('apps.users.urls', namespace='users')),

]



# 全局404页面
handler404 = 'apps.users.views.page_not_found'

#全局505页面
handler505 = 'apps.users.views.page_error'