#-*-coding:utf-8-*-


import xadmin
from xadmin import views

from .models import EmailVerifyRecord, Banner


class BaseSetting(object):
    '''主题功能'''
    enable_themes = True
    use_bootswatch = True  #调出主题菜单


class GlobalSettings(object):
    '''对全局的配置'''
    site_title = "在线IT学习管理系统"
    site_footer = "在线IT学习平台"
    menu_style = 'accordion'

class EmailVerifyRecordAdmin(object):
    #页面将会以列方式展示
    list_display = ['code','email','send_type','send_time']#使用数组，若用元组的话需要在每个元素写完后加上逗号，否则会报错
    search_fields = ['code','email','send_type','send_time']#增加搜索功能字段
    list_filter = ['code','email','send_type','send_time']#增加筛选字段
    model_icon = 'fa fa-user-circle'

class BannerAdmin(object):

    list_display = ['title','image','url','index','add_time']#使用数组，若用元组的话需要在每个元素写完后加上逗号，否则会报错
    search_fields = ['title','image','url','index']#增加搜索功能字段
    list_filter = ['title','image','url','index','add_time']#增加筛选字段
    model_icon = 'fa fa-blind'

#关联注册
xadmin.site.register(EmailVerifyRecord, EmailVerifyRecordAdmin)
xadmin.site.register(Banner, BannerAdmin)
xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSettings)