
import json
from django.shortcuts import render
from django.contrib.auth import authenticate,login, logout
from django.views.generic.base import View
from django.contrib.auth.hashers import make_password
from django.http import HttpResponse, HttpResponseRedirect
from pure_pagination import Paginator, PageNotAnInteger
from django.core.urlresolvers import reverse

from .forms import LoginForm,RegisterForm,ForgetForm,ModifyPwdForm,UploadImageForm,UserInfoForm
from .models import UserProfile,EmailVerifyRecord
from apps.utils.email_send import send_register_email
from apps.utils.mixin_utils import LoginRequiredMixin
from apps.operation.models import UserCourse,UserFavorite, UserMessage
from apps.organization.models import CourseOrg, Teacher
from apps.courses.models import Course
from .models import Banner


class LogoutView(View):
    '''
    用户登出
    '''
    def get(self, request):
        logout(request)
        #重定向
        return HttpResponseRedirect(reverse("index"))

#基于类来实现
class LoginView(View):
    def get(self,request):
        return render(request, 'login.html', {})
    def post(self,request):
        login_form = LoginForm(request.POST)#字典传入
        if login_form.is_valid():
            user_name = request.POST.get("username", '')
            pass_word = request.POST.get("password", '')
            # 该方法为验证用户名与密码，成功返回user的model对象，否则返回None
            user = authenticate(username=user_name, password=pass_word)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse("index")) # 跳转到首页，默认将user与request注册到index中
                else:
                    return render(request,"login.html")
            else:
                return render(request,"login.html",{"msg": "User name or password error!"})
        else:
            return render(request, 'login.html', {"login_form":login_form})

#用户激活
class ActiveUserView(View):
    def get(self,request,active_code):
        # 用code在数据库中过滤处信息
        all_records = EmailVerifyRecord.objects.filter(code=active_code)
        if all_records:
            for record in all_records:
                email = record.email
                # 通过邮箱查找到对应的用户
                user = UserProfile.objects.get(email=email)
                # 激活用户
                user.is_active = True
                user.save()
        else:
            return render(request,"active_file.html")
        return render(request,"login.html")

#注册
class RegisterView(View):
    def get(self,request):
        register_form = RegisterForm()
        return render(request,'register.html',{'register_form':register_form})
    def post(self,request):
        register_form = RegisterForm(request.POST)
        print(register_form)
        if register_form.is_valid():
            user_name = request.POST.get("email", '')
            if UserProfile.objects.filter(email=user_name):
                return render(request,"register.html",{"register_form":register_form,"msg":"用户已存在"})


            pass_word = request.POST.get("password", '')
            user_profile = UserProfile()
            user_profile.username = user_name
            user_profile.email = user_name
            user_profile.is_active = False
            user_profile.password = make_password(pass_word)#密码加密
            user_profile.save()

            #写入欢迎注册
            user_message = UserMessage()
            user_message.user = user_profile.id
            user_message.message = "欢迎注册"
            user_message.save()

            send_register_email(user_name,"register")
            return  render(request,'login.html')
        else:
            return render(request,"register.html",{"register_form":register_form})



class ForgetPwdView(View):
    def get(self,request):
        forget_form = ForgetForm()
        return render(request,"forgetpwd.html",{"forget_form":forget_form})

    def post(self,request):
        forget_form = ForgetForm(request.POST)
        print(forget_form)
        if forget_form.is_valid():
            email = request.POST.get("email","")
            send_register_email(email,"forget")
            return render(request,"send_seccess.html")


#重置密码
class ResetView(View):
    def get(self,request,active_code):
        # 用code在数据库中过滤处信息
        all_records = EmailVerifyRecord.objects.filter(code=active_code)
        if all_records:
            for record in all_records:
                email = record.email
                return render(request,"password_reset.html",{"email":email})#传递email判断是哪一个用户修改密码
        else:
            return render(request,"active_file.html")
        return render(request,"login.html")

#重置密码
class ModifyPwdView(View):
    '''
    用户修改密码
    '''
    def post(self,request):
        modify_form = ModifyPwdForm(request.POST)
        if modify_form.is_valid():
            pwd1 = request.POST.get("password1","")
            pwd2 = request.POST.get("password2","")
            email = request.POST.get("email","")
            if pwd1 != pwd2:
                return render(request,"password_reset.html",{"email":email,"msg":"密码不一致"})
            user = UserProfile.objects.get(email=email)
            user.password = make_password(password=pwd2)
            user.save()

            return render(request,"login.html")
        else:
            email = UserProfile.objects.get("email","")
            return render(request,"password_reset.html",{"email":email,"modify_form":modify_form})



class UserInfoView(LoginRequiredMixin,View):
    '''
    用户个人信息
    '''
    def get(self,request):
        return render(request, 'usercenter-info.html')

    # 用户修改昵称，手机号，地址，生日
    def post(self, request):
        user_info_form = UserInfoForm(request.POST, instance=request.user)

        res = dict()
        if user_info_form.is_valid():
            user_info_form.save()
            res['status'] = 'success'
        else:
            res = user_info_form.errors
        return HttpResponse(json.dumps(res), content_type='application/json')

class UploadImageView(LoginRequiredMixin,View):
    '''
    用户头像修改
    '''
    def post(self,request):
        image_form = UploadImageForm(request.POST, request.FILES,instance=request.user)#request.FILE 这里需要注意的是用户的头像是保存在 request.FILES中的
        if image_form.is_valid():
            image_form.save()
            return HttpResponse("{'status':'success'}", content_type='application/json')
        else:
            return HttpResponse("{'status':'fail'}", content_type = 'application/json')


class UpdatePwdView(View):
    '''
    个人中心修改用户密码
    '''

    def post(self, request):
        modify_form = ModifyPwdForm(request.POST)
        res = dict()

        if modify_form.is_valid():
            pwd1 = request.POST.get('password1', '')
            pwd2 = request.POST.get('password2', '')
            if pwd1 != pwd2:
                res['status'] = 'fail'
                res['msg'] = '两次密码不一致'
                return HttpResponse(json.dumps(res), content_type='application/json')

            user = request.user
            user.password = make_password(pwd2)
            user.save()

            res['status'] = 'success'
            res['msg'] = '密码修改成功'
        else:
            res = modify_form.errors

        return HttpResponse(json.dumps(res), content_type='application/json')


class SendEmailCodeView(LoginRequiredMixin,View):
    '''
    发送邮箱验证码
    '''
    def get(self,request):
        email = request.GET.get('email','')
        res = dict()
        if UserProfile.objects.filter(email=email):
            res['email'] = '邮箱已注册'
            return HttpResponse(json.dumps(res), content_type='application/json')
        send_register_email(email,"update_email")
        res['status'] = 'success'
        res['msg'] = '发送验证码成功'
        return HttpResponse(json.dumps(res), content_type='application/json')


class UpdateEmailView(LoginRequiredMixin,View):
    '''
    修改个人邮箱
    '''
    def post(self,request):
        email = request.POST.get('email','')
        code = request.POST.get('code','')
        res = dict()
        existed_recoeds = EmailVerifyRecord.objects.filter(email=email,code=code,send_type='update_email')
        if existed_recoeds:
            user = request.user
            user.email = email
            user.save()
            res['msg'] = '发送验证码成功'
            return HttpResponse(json.dumps(res), content_type='application/json')
        else:
            res['email'] = '验证码出错'
            return HttpResponse(json.dumps(res), content_type='application/json')



class MyCourseView(LoginRequiredMixin, View):
    '''
    我的课程
    '''
    def get(self, request):

        user_courses = UserCourse.objects.filter(user=request.user)

        return render(request, "usercenter-mycourse.html", {
            "user_courses":user_courses,
        })


class MyFavOrgView(LoginRequiredMixin, View):
    '''
    我的收藏课程机构
    '''
    def get(self, request):

        org_list = []
        fav_orgs = UserFavorite.objects.filter(user=request.user, fav_type=2)
        for fav_org in fav_orgs:
            org_id = fav_org.fav_id
            org = CourseOrg.objects.get(id=org_id)
            org_list.append(org)
        return render(request, "usercenter-fav-org.html", {
            "org_list":org_list,
        })


class MyFavTeacherView(LoginRequiredMixin, View):
    '''
    我的收藏授课讲师
    '''
    def get(self, request):
        teacher_list = []
        fav_teachers = UserFavorite.objects.filter(user=request.user, fav_type=3)
        for fav_teacher in fav_teachers:
            teacher_id = fav_teacher.fav_id
            teacher = Teacher.objects.get(id=teacher_id)
            teacher_list.append(teacher)
        return render(request, 'usercenter-fav-teacher.html', {
            'teacher_list': teacher_list,
        })



class MyFavCourseView(LoginRequiredMixin, View):
    '''
    我的收藏课程
    '''
    def get(self, request):
        course_list = []
        fav_courses = UserFavorite.objects.filter(user=request.user, fav_type=1)
        for fav_course in fav_courses:
            course_id = fav_course.fav_id
            course = Course.objects.get(id=course_id)
            course_list.append(course)
        return render(request, 'usercenter-fav-course.html', {
            'course_list': course_list,
        })



class MyMessageView(LoginRequiredMixin, View):
    '''
    我的消息
    '''
    def get(self, request):

        all_message = UserMessage.objects.filter(user=request.user.id)

        #点击个人消息进入后将其数字清空
        all_unread_message = UserMessage.objects.filter(user=request.user.id, has_read=False)
        for unread_message in all_unread_message:
            unread_message.has_read = True
            unread_message.save()


        #对个人消息进行分页
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        # Provide Paginator with the request object for complete querystring generation
        p = Paginator(all_message, 5, request=request)#此处需要加参数5或者其他，否则报错，文档中没有加
        messages = p.page(page)

        return render(request, 'usercenter-message.html', {
            "messages":messages,
        })



class IndexView(View):
    '''
    首页
    '''
    def get(self, request):
        #取出轮播图
        all_banners = Banner.objects.all().order_by("index")
        courses = Course.objects.filter(is_banner=False)[:6]
        banner_courses = Course.objects.filter(is_banner=False)[:3]
        course_orgs = CourseOrg.objects.all()[:15]
        return render(request, "index.html", {
            'all_banners':all_banners,
            'courses':courses,
            'banner_courses':banner_courses,
            'course_orgs':course_orgs,
        })




def page_not_found(request):
    '''
    返回404页面
    '''
    from django.shortcuts import render_to_response
    response = render_to_response('404.html', { })
    response.status_code = 404
    return response


def page_error(request):
    '''
    返回500页面
    '''
    from django.shortcuts import render_to_response
    response = render_to_response('500.html', { })
    response.status_code = 500
    return response









