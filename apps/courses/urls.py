#-*-coding:utf-8-*-



from django.conf.urls import url,include

from .views import CourseListView,CourseDetaiView,CourseInfoView,CommentsView,AddCommentsView,VideoPlayView

urlpatterns = [
    #课程列表页
    url('^list/$',CourseListView.as_view(), name='course_list'),

    #课程详情页
    url('^detail/(?P<course_id>\d+)/$',CourseDetaiView.as_view(), name='course_detail'),

    # 课程信息视频页
    url('^info/(?P<course_id>\d+)/$', CourseInfoView.as_view(), name='course_info'),

    # 课程信息视频页
    url('^comment/(?P<course_id>\d+)/$', CommentsView.as_view(), name='course_comment'),

    # 添加课程评论
    url('^add_comment/$', AddCommentsView.as_view(), name='add_comment'),#course_id不需要写，因为做post操作已经携带

    # 课程视频播放页
    url('^video/(?P<video_id>\d+)/$', VideoPlayView.as_view(), name='video_play'),
]
