#-*-coding:utf-8-*-
__author__ = 'kerberos'
__date__ = '2017/10/8 21:39 '

import xadmin

from .models import Course, Lesson, Video, CourseResource, BannerCourse


# 添加课程的时候可以添加章节
class LessonInline(object):
    model = Lesson
    extra = 0


# 添加课程的时候添加课程资源
class CourseResourceInline(object):
    model = CourseResource
    extra = 0


class CourseAdmin(object):

    list_display = ['name','desc','detail','degree','learn_times','students','fav_num','image','click_nums','add_time', 'get_zj_nums', 'go_to']
    search_fields =  ['name','desc','detail','degree','learn_times','students','fav_num','image','click_nums']
    list_filter =  ['name','desc','detail','degree','learn_times','students','fav_num','image','click_nums','add_time']
    ordering = ['-click_nums']
    readonly_fields = ['click_nums']
    list_editable = ['degree', 'desc']  #可编辑
    exclude = ['fav_nums']
    inlines = [LessonInline, CourseResourceInline] #将添加章节组装进来

    #非轮播图的课程
    def queryset(self):
        qs = super(CourseAdmin, self).queryset()
        qs = qs.filter(is_banner=False)
        return qs


class BannerCourseAdmin(object):

    list_display = ['name','desc','detail','degree','learn_times','students','fav_num','image','click_nums','add_time']
    search_fields =  ['name','desc','detail','degree','learn_times','students','fav_num','image','click_nums']
    list_filter =  ['name','desc','detail','degree','learn_times','students','fav_num','image','click_nums','add_time']
    ordering = ['-click_nums']
    readonly_fields = ['click_nums']
    exclude = ['fav_nums']
    inlines = [LessonInline, CourseResourceInline]

    #数据的过滤查询，只显示is_banner的数据
    def queryset(self):
        qs = super(BannerCourseAdmin, self).queryset()
        qs = qs.filter(is_banner=True)
        return qs



class LessonAdmin(object):

    list_display = ['course','name','add_time']
    search_fields = ['course','name']
    list_filter =  ['course__name','name','add_time']#course__name 双下划线显示外键，显示nane字段


class VideoAdmin(object):

    list_display = ['lesson','name','add_time']
    search_fields = ['lesson','name']
    list_filter =  ['lesson','name','add_time']


class CourseResourceAdmin(object):
    list_display = ['course','name','download','add_time']
    search_fields = ['course','name','download']
    list_filter =  ['course__name','name','download','add_time']


xadmin.site.register(Course, CourseAdmin)
xadmin.site.register(BannerCourse, BannerCourseAdmin)
xadmin.site.register(Lesson, LessonAdmin)
xadmin.site.register(Video, VideoAdmin)
xadmin.site.register(CourseResource, CourseResourceAdmin)