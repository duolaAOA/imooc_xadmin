#-*-coding:utf-8-*-
__author__ = 'kerberos'
__date__ = '2017/10/29 14:46 '

import re
from django import forms

from apps.operation.models import UserAsk


class UserAskForm(forms.ModelForm):
    class Meta:
        model = UserAsk
        fields = ['name', 'mobile', 'course_name']

    def clean_mobile(self):
        '''
        书写验证函数必须是以clean开头
        '''
        mobile = self.cleaned_data["mobile"]
        #正则表达式验证手机号码是否合法
        REGEX_MOBILE = "^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$"
        p = re.compile(REGEX_MOBILE)
        if p.match(mobile):
            return mobile
        else:
            raise forms.ValidationError("手机号码非法",code="mobile_invalid")