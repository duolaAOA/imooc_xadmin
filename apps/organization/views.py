# -*- coding:utf-8 -*-
from django.shortcuts import render
from django.views.generic import View
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.db.models import Q

from .models import CourseOrg,CityDity,Teacher
from .forms import UserAskForm
from apps.courses.models import Course
from apps.operation.models import UserFavorite

# Create your views here.

class OrgView(View):
    '''
    课程机构列表功能
    '''
    def get(self,request):
        # 课程机构
        all_orgs = CourseOrg.objects.all()
        #热门点击排名
        hot_orgs = all_orgs.order_by("-click_num")[:3]#排序查询
        # 城市
        all_citys = CityDity.objects.all()

        #机构搜索
        search_keywords = request.GET.get('keywords',"")
        if search_keywords:
            all_orgs = all_orgs.filter(Q(name__icontains=search_keywords)|Q(desc__icontains=search_keywords)) #sql的like语句操作，加 i (name__icontains)表示不区

        #取出筛选城市
        city_id = request.GET.get('city',"")
        if city_id:
            all_orgs = all_orgs.filter(city_id=int(city_id))#city_id为数据库中存储的字段

        #类别筛选
        category = request.GET.get('ct',"")
        if category:
            all_orgs = all_orgs.filter(category=category)

        #对学习人数与课程数的排序
        sort = request.GET.get('sort', "")
        if sort:
            if sort == "students":
                all_orgs = all_orgs.order_by("-students")
            elif sort == "courses":
                all_orgs = all_orgs.order_by("-course_nums")


        #对课程机构进行分页
        org_num = all_orgs.count()
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        # Provide Paginator with the request object for complete querystring generation
        p = Paginator(all_orgs, 5, request=request)#此处需要加参数5或者其他，否则报错，文档中没有加
        orgs = p.page(page)
        return render(request, 'org-list.html', {
            "all_orgs" : orgs,
            "all_citys" : all_citys,
            "org_num" : org_num,
            "city_id" : city_id,
            "category" : category,
            "hot_orgs" : hot_orgs,
            "sort" : sort,
        })


class AddUserAskView(View):
    '''
    用户添加咨询
    '''
    def post(self,request):
        userask_form = UserAskForm(request.POST)
        print(userask_form)
        if userask_form.is_valid():
            user_ask = userask_form.save(commit=True)#若不设置True，则仅是提交数据库不进行保存
            return HttpResponse('{"status":"success"}', content_type='application/json')
        else:
            return HttpResponse('{"status":"fail","msg":"添加出错"}',content_type="application/json")



class OrgHomeView(View):
    '''
    机构首页
    '''
    def get(self,request,org_id):
        current_page = "home"
        course_org = CourseOrg.objects.get(id=int(org_id))
        course_org.click_num +=1
        course_org.save()
        #判断用户是否收藏
        has_fav = False
        if request.user.is_authenticated():
            if UserFavorite.objects.filter(user=request.user,fav_id=course_org.id,fav_type=2):
                has_fav = True

        all_courses = course_org.course_set.all()[:3]
        '''
        course_set是CourseOrg的内置变量，由Course建立外键指向CourseOrg，
        反向取course,这样course_org.course_set.all()将所有课程取出，可以用在所有有外键的地方
        '''
        all_teacher = course_org.teacher_set.all()[:1]

        return render(request,"org-detail-homepage.html",{
            "all_courses":all_courses,
            "all_teacher":all_teacher,
            "course_org":course_org,
            "current_page": current_page,
            "has_fav" : has_fav,

        })


class OrgCourseView(View):
    '''
    机构课程列表页
    '''
    def get(self,request,org_id):
        current_page = "course"
        course_org = CourseOrg.objects.get(id=int(org_id))
        #判断用户是否收藏
        has_fav = False
        if request.user.is_authenticated():
            if UserFavorite.objects.filter(user=request.user,fav_id=course_org.id,fav_type=2):
                has_fav = True
        all_courses = course_org.course_set.all()
        '''
        course_set是CourseOrg的内置变量，由Course建立外键指向CourseOrg，
        反向取course,这样course_org.course_set.all()将所有课程取出，可以用在所有有外键的地方
        '''
        return render(request,"org-detail-course.html",{
            "all_courses":all_courses,
            "course_org":course_org,
            "current_page":current_page,
            "has_fav":has_fav,
        })


class OrgDescView(View):
    '''
    结构介绍页
    '''
    def get(self,request,org_id):
        current_page = "desc"
        course_org = CourseOrg.objects.get(id=int(org_id))
        #判断用户是否收藏
        has_fav = False
        if request.user.is_authenticated():
            if UserFavorite.objects.filter(user=request.user,fav_id=course_org.id,fav_type=2):
                has_fav = True
        '''
        course_set是CourseOrg的内置变量，由Course建立外键指向CourseOrg，
        反向取course,这样course_org.course_set.all()将所有课程取出，可以用在所有有外键的地方
        '''
        return render(request,"org-detail-desc.html",{

            "course_org":course_org,
            "current_page":current_page,
            "has_fav":has_fav,
        })


class OrgTeacherView(View):
    '''
    机构讲师
    '''
    def get(self,request,org_id):
        current_page = "teacher"
        course_org = CourseOrg.objects.get(id=int(org_id))
        #判断用户是否收藏
        has_fav = False
        if request.user.is_authenticated():
            if UserFavorite.objects.filter(user=request.user,fav_id=course_org.id,fav_type=2):
                has_fav = True
        all_teachers = course_org.teacher_set.all()

        '''
        course_set是CourseOrg的内置变量，由Course建立外键指向CourseOrg，
        反向取course,这样course_org.course_set.all()将所有课程取出，可以用在所有有外键的地方
        '''
        return render(request,"org-detail-teachers.html",{
            "all_teachers":all_teachers,
            "course_org":course_org,
            "current_page":current_page,
            "has_fav":has_fav,

        })



class AddFavView(View):
    '''
    用户收藏,=用户取消收藏
    '''
    def post(self,request):
        fav_id = request.POST.get("fav_id",0)#前台ajax传递过来
        fav_type = request.POST.get("fav_type",0)
        #判断用户是否登录
        # request中存放匿名user
        if not request.user.is_authenticated():
            #返回一个错误并应该跳转到登录页面，跳转由ajax完成，此处只需要定义status表名状态
            return HttpResponse('{"status":"fail","msg":"用户未登录"}', content_type="application/json")

        exist_records =  UserFavorite.objects.filter(user=request.user, fav_id=int(fav_id), fav_type=int(fav_type))
        #强制转换为Int，上面需要设置默认值为0，否则传过来的空串会出异常
        '''fav_type=fav_type做联合查询，如果收藏唯一讲师与课程id均为1时会提示已经收藏'''
        if exist_records:
            '''如果记录已经存在，则表示用户取消收藏'''
            exist_records.delete()#全部删除
            if int(fav_type) == 1:
                course = Course.objects.get(id=int(fav_id))
                course.fav_num -=1
                if course.fav_num < 0:
                    course.fav_num = 0
                course.save()

            elif int(fav_type) == 2:
                course_org = CourseOrg.objects.get(id=int(fav_id))
                course_org.fav_num -= 1
                if course_org.fav_num < 0:
                    course_org.fav_num = 0
                course_org.save()

            elif int(fav_type) == 3:
                teacher = Teacher.objects.get(id=int(fav_id))
                teacher.fav_num -=1
                if teacher.fav_num < 0:
                    teacher.fav_num = 0
                teacher.save()


            return HttpResponse('{"status":"fail","msg":"收藏"}', content_type="application/json")
        else:
            user_fav = UserFavorite()
            if int(fav_id) > 0 and int(fav_type) > 0:
                user_fav.user = request.user  #这个user需要添加
                user_fav.fav_id = int(fav_id)
                user_fav.fav_type = int(fav_type)
                user_fav.save()

                if int(fav_type) == 1:
                    course = Course.objects.get(id=int(fav_id))
                    course.fav_num += 1
                    course.save()

                elif int(fav_type) == 2:
                    course_org = CourseOrg.objects.get(id=int(fav_id))
                    course_org.fav_num += 1
                    course_org.save()

                elif int(fav_type) == 3:
                    teacher = Teacher.objects.get(id=int(fav_id))
                    teacher.fav_num += 1
                    teacher.save()

                return HttpResponse('{"status":"success","msg":"已收藏"}', content_type="application/json")
            else:
                return HttpResponse('{"status":"fail","msg":"收藏出错"}', content_type="application/json")



class TeacherListView(View):
    '''
    课程讲师列表页
    '''
    def get(self,request):
        all_teachers = Teacher.objects.all()

        #机构搜索
        search_keywords = request.GET.get('keywords',"")
        if search_keywords:
            all_teachers = all_teachers.filter(Q(name__icontains=search_keywords)|
                                               Q(work_company__icontains=search_keywords)|
                                               Q(work_postion=search_keywords))

        #排序功能
        sort = request.GET.get('sort', "")
        if sort:
            if sort == "hot":
                all_teachers = all_teachers.order_by("-click_nums")

        sorted_teacher = Teacher.objects.all().order_by("-click_nums")[:3]
        teachers_num = all_teachers.count()
        #对授课讲师进行分页
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1
        # Provide Paginator with the request object for complete querystring generation
        p = Paginator(all_teachers, 5, request=request)#此处需要加参数5或者其他，否则报错，文档中没有加
        teachers = p.page(page)

        return render(request,"teachers-list.html",{
            "all_teachers":teachers,
            "sorted_teacher":sorted_teacher,
            "sort":sort,
            "teachers_num":teachers_num
        })


class TeacherDetailView(View):
    def get(self,request,teacher_id):
        teacher = Teacher.objects.get(id=int(teacher_id))
        teacher.click_nums +=1
        teacher.save()
        all_courses = Course.objects.filter(teacher=teacher)

        has_teacher_faved = False
        if UserFavorite.objects.filter(user=request.user,fav_type=3,fav_id=teacher.id):
            has_teacher_faved = True

        has_org_faved = False
        if UserFavorite.objects.filter(user=request.user,fav_type=2,fav_id=teacher.org.id):
            has_org_faved = True

        #讲师排行
        sorted_teacher = Teacher.objects.all().order_by("-click_nums")[:3]
        return render(request,"teacher-detail.html",{
            "teacher":teacher,
            "all_courses":all_courses,
            "sorted_teacher":sorted_teacher,
            "has_teacher_faved":has_teacher_faved,
            "has_org_faved":has_org_faved,


        })