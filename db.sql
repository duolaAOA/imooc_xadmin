/*
MySQL Backup
Source Server Version: 5.7.19
Source Database: imooc
Date: 2017/11/30 20:16:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `auth_group_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `auth_permission`
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `auth_user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `auth_user_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `captcha_captchastore`
-- ----------------------------
DROP TABLE IF EXISTS `captcha_captchastore`;
CREATE TABLE `captcha_captchastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `challenge` varchar(32) NOT NULL,
  `response` varchar(32) NOT NULL,
  `hashkey` varchar(40) NOT NULL,
  `expiration` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashkey` (`hashkey`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `courses_course`
-- ----------------------------
DROP TABLE IF EXISTS `courses_course`;
CREATE TABLE `courses_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `detail` longtext NOT NULL,
  `degree` varchar(2) NOT NULL,
  `learn_times` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `fav_num` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `click_nums` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_org_id` int(11) DEFAULT NULL,
  `category` varchar(20) NOT NULL,
  `tag` varchar(16) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `teacher_tell` varchar(500) NOT NULL,
  `youneed_know` varchar(500) NOT NULL,
  `is_banner` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_course_11456c5a` (`course_org_id`),
  KEY `courses_course_d9614d40` (`teacher_id`),
  CONSTRAINT `courses_cour_course_org_id_4d2c4aab_fk_organization_courseorg_id` FOREIGN KEY (`course_org_id`) REFERENCES `organization_courseorg` (`id`),
  CONSTRAINT `courses_course_teacher_id_846fa526_fk_organization_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `organization_teacher` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `courses_courseresource`
-- ----------------------------
DROP TABLE IF EXISTS `courses_courseresource`;
CREATE TABLE `courses_courseresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `download` varchar(100) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_courseresource_ea134da7` (`course_id`),
  CONSTRAINT `courses_courseresource_course_id_5eba1332_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `courses_lesson`
-- ----------------------------
DROP TABLE IF EXISTS `courses_lesson`;
CREATE TABLE `courses_lesson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_lesson_course_id_16bc4882_fk_courses_course_id` (`course_id`),
  CONSTRAINT `courses_lesson_course_id_16bc4882_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `courses_video`
-- ----------------------------
DROP TABLE IF EXISTS `courses_video`;
CREATE TABLE `courses_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `learn_times` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courses_video_lesson_id_59f2396e_fk_courses_lesson_id` (`lesson_id`),
  CONSTRAINT `courses_video_lesson_id_59f2396e_fk_courses_lesson_id` FOREIGN KEY (`lesson_id`) REFERENCES `courses_lesson` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `django_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `django_content_type`
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `django_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `django_session`
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `operation_coursecomments`
-- ----------------------------
DROP TABLE IF EXISTS `operation_coursecomments`;
CREATE TABLE `operation_coursecomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comments` varchar(500) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_coursecomments_course_id_c88f1b6a_fk_courses_course_id` (`course_id`),
  KEY `operation_coursecomment_user_id_f5ff70b3_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `operation_coursecomment_user_id_f5ff70b3_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`),
  CONSTRAINT `operation_coursecomments_course_id_c88f1b6a_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `operation_userask`
-- ----------------------------
DROP TABLE IF EXISTS `operation_userask`;
CREATE TABLE `operation_userask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `course_name` varchar(50) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `operation_usercourse`
-- ----------------------------
DROP TABLE IF EXISTS `operation_usercourse`;
CREATE TABLE `operation_usercourse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `add_time` datetime(6) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_usercourse_course_id_9f1eab2e_fk_courses_course_id` (`course_id`),
  KEY `operation_usercourse_user_id_835fe81a_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `operation_usercourse_course_id_9f1eab2e_fk_courses_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses_course` (`id`),
  CONSTRAINT `operation_usercourse_user_id_835fe81a_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `operation_userfavorite`
-- ----------------------------
DROP TABLE IF EXISTS `operation_userfavorite`;
CREATE TABLE `operation_userfavorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fav_id` int(11) NOT NULL,
  `fav_type` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `operation_userfavorite_user_id_ad46a6af_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `operation_userfavorite_user_id_ad46a6af_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `operation_usermessage`
-- ----------------------------
DROP TABLE IF EXISTS `operation_usermessage`;
CREATE TABLE `operation_usermessage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `has_read` tinyint(1) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `organization_citydity`
-- ----------------------------
DROP TABLE IF EXISTS `organization_citydity`;
CREATE TABLE `organization_citydity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `desc` varchar(200) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `organization_courseorg`
-- ----------------------------
DROP TABLE IF EXISTS `organization_courseorg`;
CREATE TABLE `organization_courseorg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `desc` longtext NOT NULL,
  `click_num` int(11) NOT NULL,
  `fav_num` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `address` varchar(150) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `city_id` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `course_nums` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `tag` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organization_course_city_id_4a842f85_fk_organization_citydity_id` (`city_id`),
  CONSTRAINT `organization_course_city_id_4a842f85_fk_organization_citydity_id` FOREIGN KEY (`city_id`) REFERENCES `organization_citydity` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `organization_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `organization_teacher`;
CREATE TABLE `organization_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `work_years` int(11) NOT NULL,
  `work_company` varchar(50) NOT NULL,
  `work_postion` varchar(50) NOT NULL,
  `points` varchar(50) NOT NULL,
  `click_nums` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `org_id` int(11) NOT NULL,
  `fav_nums` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `organization_teache_org_id_cd000a1a_fk_organization_courseorg_id` (`org_id`),
  CONSTRAINT `organization_teache_org_id_cd000a1a_fk_organization_courseorg_id` FOREIGN KEY (`org_id`) REFERENCES `organization_courseorg` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users_banner`
-- ----------------------------
DROP TABLE IF EXISTS `users_banner`;
CREATE TABLE `users_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `index` int(11) NOT NULL,
  `add_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users_emailverifyrecord`
-- ----------------------------
DROP TABLE IF EXISTS `users_emailverifyrecord`;
CREATE TABLE `users_emailverifyrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `send_type` varchar(20) NOT NULL,
  `send_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users_userprofile`
-- ----------------------------
DROP TABLE IF EXISTS `users_userprofile`;
CREATE TABLE `users_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `nick_name` varchar(50) NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(6) NOT NULL,
  `address` varchar(100) NOT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users_userprofile_groups`
-- ----------------------------
DROP TABLE IF EXISTS `users_userprofile_groups`;
CREATE TABLE `users_userprofile_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_userprofile_groups_userprofile_id_823cf2fc_uniq` (`userprofile_id`,`group_id`),
  KEY `users_userprofile_groups_group_id_3de53dbf_fk_auth_group_id` (`group_id`),
  CONSTRAINT `users_userprofil_userprofile_id_a4496a80_fk_users_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `users_userprofile` (`id`),
  CONSTRAINT `users_userprofile_groups_group_id_3de53dbf_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `users_userprofile_user_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `users_userprofile_user_permissions`;
CREATE TABLE `users_userprofile_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userprofile_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_userprofile_user_permissions_userprofile_id_d0215190_uniq` (`userprofile_id`,`permission_id`),
  KEY `users_userprofile_u_permission_id_393136b6_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `users_userprofil_userprofile_id_34544737_fk_users_userprofile_id` FOREIGN KEY (`userprofile_id`) REFERENCES `users_userprofile` (`id`),
  CONSTRAINT `users_userprofile_u_permission_id_393136b6_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `xadmin_bookmark`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_bookmark`;
CREATE TABLE `xadmin_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `url_name` varchar(64) NOT NULL,
  `query` varchar(1000) NOT NULL,
  `is_share` tinyint(1) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_bookma_content_type_id_60941679_fk_django_content_type_id` (`content_type_id`),
  KEY `xadmin_bookmark_user_id_42d307fc_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_bookma_content_type_id_60941679_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `xadmin_bookmark_user_id_42d307fc_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `xadmin_log`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_log`;
CREATE TABLE `xadmin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `ip_addr` char(39) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` varchar(32) NOT NULL,
  `message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_log_content_type_id_2a6cb852_fk_django_content_type_id` (`content_type_id`),
  KEY `xadmin_log_user_id_bb16a176_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_log_content_type_id_2a6cb852_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `xadmin_log_user_id_bb16a176_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `xadmin_usersettings`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_usersettings`;
CREATE TABLE `xadmin_usersettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(256) NOT NULL,
  `value` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_usersettings_user_id_edeabe4a_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_usersettings_user_id_edeabe4a_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `xadmin_userwidget`
-- ----------------------------
DROP TABLE IF EXISTS `xadmin_userwidget`;
CREATE TABLE `xadmin_userwidget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` varchar(256) NOT NULL,
  `widget_type` varchar(50) NOT NULL,
  `value` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `xadmin_userwidget_user_id_c159233a_fk_users_userprofile_id` (`user_id`),
  CONSTRAINT `xadmin_userwidget_user_id_c159233a_fk_users_userprofile_id` FOREIGN KEY (`user_id`) REFERENCES `users_userprofile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1','Can add log entry','1','add_logentry'), ('2','Can change log entry','1','change_logentry'), ('3','Can delete log entry','1','delete_logentry'), ('4','Can add permission','2','add_permission'), ('5','Can change permission','2','change_permission'), ('6','Can delete permission','2','delete_permission'), ('7','Can add group','3','add_group'), ('8','Can change group','3','change_group'), ('9','Can delete group','3','delete_group'), ('13','Can add content type','5','add_contenttype'), ('14','Can change content type','5','change_contenttype'), ('15','Can delete content type','5','delete_contenttype'), ('16','Can add session','6','add_session'), ('17','Can change session','6','change_session'), ('18','Can delete session','6','delete_session'), ('19','Can add 用户信息','7','add_userprofile'), ('20','Can change 用户信息','7','change_userprofile'), ('21','Can delete 用户信息','7','delete_userprofile'), ('22','Can add 邮箱验证码','8','add_emailverifyrecord'), ('23','Can change 邮箱验证码','8','change_emailverifyrecord'), ('24','Can delete 邮箱验证码','8','delete_emailverifyrecord'), ('25','Can add 轮播图','9','add_banner'), ('26','Can change 轮播图','9','change_banner'), ('27','Can delete 轮播图','9','delete_banner'), ('28','Can add 城市','10','add_citydity'), ('29','Can change 城市','10','change_citydity'), ('30','Can delete 城市','10','delete_citydity'), ('31','Can add 课程机构','11','add_courseorg'), ('32','Can change 课程机构','11','change_courseorg'), ('33','Can delete 课程机构','11','delete_courseorg'), ('34','Can add 教师','12','add_teacher'), ('35','Can change 教师','12','change_teacher'), ('36','Can delete 教师','12','delete_teacher'), ('37','Can add 课程','13','add_course'), ('38','Can change 课程','13','change_course'), ('39','Can delete 课程','13','delete_course'), ('40','Can add 章节','14','add_lesson'), ('41','Can change 章节','14','change_lesson'), ('42','Can delete 章节','14','delete_lesson'), ('43','Can add 视频','15','add_video'), ('44','Can change 视频','15','change_video'), ('45','Can delete 视频','15','delete_video'), ('46','Can add 课程资源','16','add_courseresource'), ('47','Can change 课程资源','16','change_courseresource'), ('48','Can delete 课程资源','16','delete_courseresource'), ('49','Can add 用户咨询','17','add_userask'), ('50','Can change 用户咨询','17','change_userask'), ('51','Can delete 用户咨询','17','delete_userask'), ('52','Can add 课程评论','18','add_coursecomments'), ('53','Can change 课程评论','18','change_coursecomments'), ('54','Can delete 课程评论','18','delete_coursecomments'), ('55','Can add 用户收藏','19','add_userfavorite'), ('56','Can change 用户收藏','19','change_userfavorite'), ('57','Can delete 用户收藏','19','delete_userfavorite'), ('58','Can add 用户消息','20','add_usermessage'), ('59','Can change 用户消息','20','change_usermessage'), ('60','Can delete 用户消息','20','delete_usermessage'), ('61','Can add 用户消息','21','add_usercourse'), ('62','Can change 用户消息','21','change_usercourse'), ('63','Can delete 用户消息','21','delete_usercourse'), ('64','Can view log entry','1','view_logentry'), ('65','Can view group','3','view_group'), ('66','Can view permission','2','view_permission'), ('67','Can view content type','5','view_contenttype'), ('68','Can view 课程','13','view_course'), ('69','Can view 课程资源','16','view_courseresource'), ('70','Can view 章节','14','view_lesson'), ('71','Can view 视频','15','view_video'), ('72','Can view 课程评论','18','view_coursecomments'), ('73','Can view 用户咨询','17','view_userask'), ('74','Can view 用户消息','21','view_usercourse'), ('75','Can view 用户收藏','19','view_userfavorite'), ('76','Can view 用户消息','20','view_usermessage'), ('77','Can view 城市','10','view_citydity'), ('78','Can view 课程机构','11','view_courseorg'), ('79','Can view 教师','12','view_teacher'), ('80','Can view session','6','view_session'), ('81','Can view 轮播图','9','view_banner'), ('82','Can view 邮箱验证码','8','view_emailverifyrecord'), ('83','Can view 用户信息','7','view_userprofile'), ('84','Can add Bookmark','22','add_bookmark'), ('85','Can change Bookmark','22','change_bookmark'), ('86','Can delete Bookmark','22','delete_bookmark'), ('87','Can add User Setting','23','add_usersettings'), ('88','Can change User Setting','23','change_usersettings'), ('89','Can delete User Setting','23','delete_usersettings'), ('90','Can add User Widget','24','add_userwidget'), ('91','Can change User Widget','24','change_userwidget'), ('92','Can delete User Widget','24','delete_userwidget'), ('93','Can add log entry','25','add_log'), ('94','Can change log entry','25','change_log'), ('95','Can delete log entry','25','delete_log'), ('96','Can view Bookmark','22','view_bookmark'), ('97','Can view log entry','25','view_log'), ('98','Can view User Setting','23','view_usersettings'), ('99','Can view User Widget','24','view_userwidget'), ('100','Can add captcha store','26','add_captchastore'), ('101','Can change captcha store','26','change_captchastore'), ('102','Can delete captcha store','26','delete_captchastore'), ('103','Can view captcha store','26','view_captchastore');
INSERT INTO `captcha_captchastore` VALUES ('124','HEPI','hepi','bc68b4495a3c32da9cf950d9e473b39b4477ba9e','2017-10-25 21:44:56.674033'), ('125','WSSU','wssu','92d2146d4e8ebc38e32ed41abc655f6f025ed607','2017-10-25 21:45:10.484878'), ('126','CMME','cmme','4e6881f7198e2a1eb0210c903cdbb1d134075fbc','2017-10-25 21:45:43.037758'), ('127','GJPG','gjpg','6a03aa2fd3984bab1e83486dc632fe405bad6045','2017-10-25 21:45:47.936246'), ('128','WVUF','wvuf','729ce58150b941361431068d55975da80110ce03','2017-10-25 21:46:11.498034'), ('129','IRYZ','iryz','a3216b0215fe075cc706e8c96b481d533124df7f','2017-10-26 20:04:17.860169'), ('130','HRFP','hrfp','a6a17cecc75a4742763be7dfb70fcbef1e060c98','2017-10-26 20:04:44.507352'), ('131','ATYU','atyu','da37165d1378fc086961aa9e8f37d6b3af96d5c8','2017-10-26 20:30:04.155739'), ('132','FQQP','fqqp','e014eb1ba5f9f6f4ae6a580a4b86c78b81730f2e','2017-11-22 21:43:23.771748'), ('133','KIED','kied','e58b10b463ecd63e88274261dbccbb20787913ba','2017-11-22 21:44:01.456573');
INSERT INTO `courses_course` VALUES ('1','django','django入门','25小时企业级实战：从0到项目成型\r\n以互联网公司标准开发流程，从零开发出一套可以达到上线标准的在线教育平台\r\n得到一个接近慕课网的在线教育平台\r\n开发一个全新在线教育平台——慕学网，具备一个在线教育网站所应该具有的\r\n所有功能，单是这套系统的完整代码就值得你立刻入手这门课程\r\ndjango综合运用：覆盖所有常用模块\r\nsettings配置、 url配置、 view编码、 model设计、 modelform表单验证、 \r\ntemplates模板、 django常用内置函数，以及通用的django开发库','zj','3453','34535','3234','course/2017/m/540e57300001d6d906000338-240-135_MSIqfvw.jpg','23182','2017-10-31 19:34:00.000000','3','后端开发','python','1','需要平时多加练习','django学习的基础需要有python基本的语法以及前端基本知识','1'), ('2','go语言入门','go语言入门','go语言入门go语言入门go语言入门','cj','123','32','23','course/2017/m/57035ff200014b8a06000338-240-135.jpg','47','2017-11-01 20:24:00.000000','3','后端开发','python',NULL,'','','0'), ('3','c++入门到放弃','c++入门到放弃','c++入门到放弃c++入门到放弃c++入门到放弃','cj','123','234','234','course/2017/m/python面向对象.jpg','4','2017-11-01 20:25:00.000000','10','后端开发','',NULL,'','','0'), ('4','无服务器架构系列讲解','python异常处理','python异常处Lambda是AWS提供的一款无服务器计算服务。您不需要管理服务器、不需要安装操作系统、不需要维护运行时环境、不需要担心应用的可用性和扩展性。本课程主要会以Demo形式讲解AWS Lambda的概念，编程模型、并发问题、版本控制 ，怎么构建Lambda函数， 配合DynamoDB进行数据库交互，配合S3进行事件触发（图片压缩、水印等），配合Kinesis就行实时流数据处理，配合SNS就行消息通知事件触发等，以及如何构建一个动态交互式网站python异常处理python异常处理','gj','1545','27','213','course/2017/m/5a13b8e2a13cf.jpg','4340','2017-11-01 20:26:00.000000','3','职业课程库','AWS','1','适合人群：  系统管理员、系统开发人员、解决方案架构师、解决方案设计工程师','软件环境：AWS 云计算环境','0'), ('5','python面向对象','python面向对象','python面向对象python面向对象python面向对象python面向对象','cj','2342','234','34','course/2017/m/python面向对象_xlOQ1KH.jpg','123','2017-11-01 20:27:00.000000','3','后端开发','django',NULL,'','','0'), ('6','AWS云设计高可用架构','本课程介绍了 Auto Scaling 服务，为什么“不要去猜测容量；启动什么样的服务器；启动服务器的方法以及增减服务器的方法。','1.介绍了 Auto Scaling 服务，为什么“不要去猜测容量以及Auto Scaling 的关键组件\r\n2.讲解Autoscaling的启动配置，组使用启动配置 作为其 EC2 实例的模板。创建启动配置时，您可以为实例指定诸如 AMI ID、实例类型、密钥对、安全组和块储存设备映射等信息','zj','3454','545','445','course/2017/m/59f1c9596091d.jpg','347','2017-11-01 20:27:00.000000','7','职业课程库','AWS','3','适合人群：  系统管理人员、系统开发人员、 解决方案架构师、解决方案设计工程师','软件环境：AWS云计算环境','0'), ('7','mysql高级教程','mysql高级教程','mysql高级教程mysql高级教程mysql高级教程','cj','45','58','456','course/2017/m/mysql.jpg','46','2017-11-01 20:28:00.000000','3','后端开发','',NULL,'','','0'), ('8','Tornado 开发--TCP 编程','Python 由于具有简洁而清晰的语法以及强大的开发效率，在 TCP 编程方面也得到广泛应用。很多开发人员选择python作为手游服务器的开发语言，其中 tornado 作为一个异步 i o框架，效率突出，因此也得到了很多开发人员的青睐。本课程主要通过对 tornado 的 TCP 方面知识的介绍，来讲解如何使用 tornado 进行 TCP 编程。','1.TCP 的简单介绍，以及 tornado 在 TCP 层里的工作\r\n2.ioloop 相关 API 介绍\r\n3.iostream 相关 API 介绍\r\n4.rpc介 绍以及使用\r\n5.同类框架 twisted 方面的介绍以及与 tornado 的比较','cj','23423','235','3434','course/2017/m/57216b7d1ac79.jpg','237','2017-11-01 20:30:00.000000','10','python基础','后端开发','2','适合人群：  手游服务器开发人员，对 TCP 编程感兴趣的','软件环境：mac','1'), ('9','微软人工智能 － 服务和API','微软认知服务包涵的智能API让你仅用几行代码就可以借助强大的算法开发应用程序。它们跨设备，跨平台，不论是 iOS、Android 或者 Windows ，你都可以轻松完成配置。','1.	微软人工智能服务和API概览\r\n2.	微软认知服务 – 视觉API、语义及语音API、自定义语音服务介绍\r\n3.	微软智能语义理解服务LUIS技术详解\r\n4.	微软对话即平台及会话机器人框架 Bot Framework\r\n5.	Vision API 在实际案例中的应用\r\n6.	Microsoft Translator - 微软人工智能机器翻译技术\r\n7.	会话机器人 BOT 实例分享','cj','3454','439','35','course/2017/m/59f6d46dcb4dc.jpg','352','2017-11-01 20:30:00.000000','9','服务与API','职业课程库','2','适合人群：  致力于人工智能的开发人员','软件环境：Microsoft Azure、Bot Framework SDK for .NET、 Bot Framework SDK for Node.js','0'), ('10','python高级编程','python高级编程','python高级编程python高级编程python高级编程','zj','324','256','234','course/2017/m/540e57300001d6d906000338-240-135_Z3HIQ2t.jpg','59','2017-11-01 20:31:00.000000','3','后端开发','···','5','···','···','0'), ('11','python数据分析实战','python数据分析实战','python数据分析实战python数据分析实战python数据分析实战','gj','32','131','213','course/2017/m/540e57300001d6d906000338-240-135_mvvGYHL.jpg','33','2017-11-01 20:31:00.000000','4','后端开发','python',NULL,'','','0');
INSERT INTO `courses_courseresource` VALUES ('1','深入_Python_3','course/resource/201711/深入_Python_3.pdf','2017-11-03 20:32:00.000000','1');
INSERT INTO `courses_lesson` VALUES ('1','第一章 基础语法','2017-11-03 20:05:00.000000','10'), ('2','第二章 进阶开发','2017-11-03 20:08:00.000000','10');
INSERT INTO `courses_video` VALUES ('1','1.1 如何使用github[tortoisegit]','2017-11-03 20:14:00.000000','1','http://oyy33o87w.bkt.clouddn.com/%E3%80%90tortoisegit%E3%80%91%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8GitHub.mp4','0'), ('2','1.2 基础概念','2017-11-03 20:15:00.000000','1','http://coding.imooc.com/class/78.html','0'), ('3','2.1 高级编程','2017-11-03 20:17:00.000000','2','http://www.imooc.com/video/15825','0'), ('4','2.2 高级处理技巧','2017-11-03 20:19:00.000000','2','http://www.imooc.com/video/15780','0');
INSERT INTO `django_content_type` VALUES ('1','admin','logentry'), ('3','auth','group'), ('2','auth','permission'), ('26','captcha','captchastore'), ('5','contenttypes','contenttype'), ('13','courses','course'), ('16','courses','courseresource'), ('14','courses','lesson'), ('15','courses','video'), ('18','operation','coursecomments'), ('17','operation','userask'), ('21','operation','usercourse'), ('19','operation','userfavorite'), ('20','operation','usermessage'), ('10','organization','citydity'), ('11','organization','courseorg'), ('12','organization','teacher'), ('6','sessions','session'), ('9','users','banner'), ('8','users','emailverifyrecord'), ('7','users','userprofile'), ('22','xadmin','bookmark'), ('25','xadmin','log'), ('23','xadmin','usersettings'), ('24','xadmin','userwidget');
INSERT INTO `django_migrations` VALUES ('1','contenttypes','0001_initial','2017-10-07 05:36:35.197094'), ('2','auth','0001_initial','2017-10-07 05:36:44.119393'), ('3','admin','0001_initial','2017-10-07 05:36:45.820956'), ('4','admin','0002_logentry_remove_auto_add','2017-10-07 05:36:45.994012'), ('5','contenttypes','0002_remove_content_type_name','2017-10-07 05:36:46.975865'), ('6','auth','0002_alter_permission_name_max_length','2017-10-07 05:36:47.640640'), ('7','auth','0003_alter_user_email_max_length','2017-10-07 05:36:48.271885'), ('8','auth','0004_alter_user_username_opts','2017-10-07 05:36:48.320396'), ('9','auth','0005_alter_user_last_login_null','2017-10-07 05:36:48.892947'), ('10','auth','0006_require_contenttypes_0002','2017-10-07 05:36:48.931078'), ('11','auth','0007_alter_validators_add_error_messages','2017-10-07 05:36:48.987778'), ('12','sessions','0001_initial','2017-10-07 05:36:49.606034'), ('13','users','0001_initial','2017-10-07 05:51:52.632234'), ('14','courses','0001_initial','2017-10-08 11:41:01.318028'), ('15','operation','0001_initial','2017-10-08 11:41:06.564762'), ('16','organization','0001_initial','2017-10-08 11:41:09.210634'), ('17','users','0002_banner_emailverifyrecord','2017-10-08 11:41:09.876111'), ('18','xadmin','0001_initial','2017-10-08 20:40:25.271116'), ('19','xadmin','0002_log','2017-10-08 20:40:27.166478'), ('20','xadmin','0003_auto_20160715_0100','2017-10-08 20:40:27.947082'), ('21','captcha','0001_initial','2017-10-22 13:36:10.889973'), ('22','courses','0002_courseresource_course','2017-10-22 13:44:27.097452'), ('23','operation','0002_auto_20171022_1344','2017-10-22 13:44:27.182512'), ('24','organization','0002_auto_20171028_2042','2017-10-28 20:42:53.918496'), ('25','organization','0003_auto_20171029_1134','2017-10-29 11:35:10.314137'), ('26','organization','0004_auto_20171031_1925','2017-10-31 19:32:07.409026'), ('27','courses','0003_course_course_org','2017-10-31 19:32:09.198292'), ('28','organization','0005_auto_20171031_2057','2017-10-31 20:58:02.729870'), ('29','organization','0006_auto_20171031_2127','2017-10-31 21:27:33.693480'), ('30','organization','0007_auto_20171101_1421','2017-11-01 14:21:59.928468'), ('31','courses','0004_course_category','2017-11-02 21:17:53.515790'), ('32','courses','0005_course_tag','2017-11-03 19:00:09.243046'), ('33','courses','0006_video_url','2017-11-03 20:13:51.089733'), ('34','courses','0007_video_learn_times','2017-11-03 20:30:22.617848'), ('35','courses','0008_course_teacher','2017-11-03 20:57:29.753780'), ('36','courses','0009_auto_20171103_2103','2017-11-03 21:03:38.979337'), ('37','organization','0008_teacher_age','2017-11-08 18:56:29.156569'), ('38','users','0003_auto_20171114_0937','2017-11-14 09:37:37.829551'), ('39','users','0004_auto_20171114_0942','2017-11-14 09:42:43.177022'), ('40','courses','0010_course_is_banner','2017-11-23 15:57:43.076480'), ('41','organization','0009_courseorg_tag','2017-11-23 17:40:11.810714');
INSERT INTO `django_session` VALUES ('dwxr5f98kmk049ox6chhc3q3jepoihma','YTFkMWQ4ZGE5MTlkNjE0NWNiODY2ZTU2N2ZiNGFiY2EzNGE5ZTNlYzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiTElTVF9RVUVSWSI6W1siY291cnNlcyIsImNvdXJzZXJlc291cmNlIl0sIiJdLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjA5OTgwMjJjOWM5NjQyMGM0YzIxYmYzMGU0YzNjZWI2MTMxN2IxYzEiLCJuYXZfbWVudSI6Ilt7XCJtZW51c1wiOiBbe1widXJsXCI6IFwiL3hhZG1pbi91c2Vycy91c2VycHJvZmlsZS9cIiwgXCJpY29uXCI6IFwiZmEgZmEtdXNlclwiLCBcIm9yZGVyXCI6IDMsIFwidGl0bGVcIjogXCJcdTc1MjhcdTYyMzdcdTRmZTFcdTYwNmZcIn0sIHtcInVybFwiOiBcIi94YWRtaW4vdXNlcnMvZW1haWx2ZXJpZnlyZWNvcmQvXCIsIFwiaWNvblwiOiBcImZhIGZhLXVzZXItY2lyY2xlXCIsIFwib3JkZXJcIjogNSwgXCJ0aXRsZVwiOiBcIlx1OTBhZVx1N2JiMVx1OWE4Y1x1OGJjMVx1NzgwMVwifSwge1widXJsXCI6IFwiL3hhZG1pbi91c2Vycy9iYW5uZXIvXCIsIFwiaWNvblwiOiBcImZhIGZhLWJsaW5kXCIsIFwib3JkZXJcIjogNiwgXCJ0aXRsZVwiOiBcIlx1OGY2ZVx1NjRhZFx1NTZmZVwifV0sIFwiZmlyc3RfaWNvblwiOiBcImZhIGZhLXVzZXJcIiwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL3VzZXJzL3VzZXJwcm9maWxlL1wiLCBcInRpdGxlXCI6IFwiXHU3NTI4XHU2MjM3XHU0ZmUxXHU2MDZmXCJ9LCB7XCJtZW51c1wiOiBbe1widXJsXCI6IFwiL3hhZG1pbi9vcGVyYXRpb24vdXNlcmFzay9cIiwgXCJpY29uXCI6IG51bGwsIFwib3JkZXJcIjogMTUsIFwidGl0bGVcIjogXCJcdTc1MjhcdTYyMzdcdTU0YThcdThiZTJcIn0sIHtcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL2NvdXJzZWNvbW1lbnRzL1wiLCBcImljb25cIjogbnVsbCwgXCJvcmRlclwiOiAxNiwgXCJ0aXRsZVwiOiBcIlx1OGJmZVx1N2EwYlx1OGJjNFx1OGJiYVwifSwge1widXJsXCI6IFwiL3hhZG1pbi9vcGVyYXRpb24vdXNlcmZhdm9yaXRlL1wiLCBcImljb25cIjogbnVsbCwgXCJvcmRlclwiOiAxNywgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NjUzNlx1ODVjZlwifSwge1widXJsXCI6IFwiL3hhZG1pbi9vcGVyYXRpb24vdXNlcm1lc3NhZ2UvXCIsIFwiaWNvblwiOiBudWxsLCBcIm9yZGVyXCI6IDE4LCBcInRpdGxlXCI6IFwiXHU3NTI4XHU2MjM3XHU2ZDg4XHU2MDZmXCJ9LCB7XCJ1cmxcIjogXCIveGFkbWluL29wZXJhdGlvbi91c2VyY291cnNlL1wiLCBcImljb25cIjogbnVsbCwgXCJvcmRlclwiOiAxOSwgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1OGJmZVx1N2EwYlwifV0sIFwiZmlyc3RfdXJsXCI6IFwiL3hhZG1pbi9vcGVyYXRpb24vdXNlcmFzay9cIiwgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NjRjZFx1NGY1Y1wifSwge1wibWVudXNcIjogW3tcInVybFwiOiBcIi94YWRtaW4veGFkbWluL2xvZy9cIiwgXCJpY29uXCI6IFwiZmEgZmEtY29nXCIsIFwib3JkZXJcIjogMjEsIFwidGl0bGVcIjogXCJcdTY1ZTVcdTVmZDdcdThiYjBcdTVmNTVcIn1dLCBcImZpcnN0X2ljb25cIjogXCJmYSBmYS1jb2dcIiwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL3hhZG1pbi9sb2cvXCIsIFwidGl0bGVcIjogXCJcdTdiYTFcdTc0MDZcIn0sIHtcIm1lbnVzXCI6IFt7XCJ1cmxcIjogXCIveGFkbWluL29yZ2FuaXphdGlvbi9jaXR5ZGl0eS9cIiwgXCJpY29uXCI6IG51bGwsIFwib3JkZXJcIjogNywgXCJ0aXRsZVwiOiBcIlx1NTdjZVx1NWUwMlwifSwge1widXJsXCI6IFwiL3hhZG1pbi9vcmdhbml6YXRpb24vY291cnNlb3JnL1wiLCBcImljb25cIjogbnVsbCwgXCJvcmRlclwiOiA4LCBcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXHU2NzNhXHU2Nzg0XCJ9LCB7XCJ1cmxcIjogXCIveGFkbWluL29yZ2FuaXphdGlvbi90ZWFjaGVyL1wiLCBcImljb25cIjogbnVsbCwgXCJvcmRlclwiOiA5LCBcInRpdGxlXCI6IFwiXHU2NTU5XHU1ZTA4XCJ9XSwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL29yZ2FuaXphdGlvbi90ZWFjaGVyL1wiLCBcInRpdGxlXCI6IFwiXHU3YmExXHU3NDA2XHU2NzNhXHU2Nzg0XCJ9LCB7XCJtZW51c1wiOiBbe1widXJsXCI6IFwiL3hhZG1pbi9hdXRoL2dyb3VwL1wiLCBcImljb25cIjogXCJmYSBmYS1ncm91cFwiLCBcIm9yZGVyXCI6IDIsIFwidGl0bGVcIjogXCJcdTdlYzRcIn0sIHtcInVybFwiOiBcIi94YWRtaW4vYXV0aC9wZXJtaXNzaW9uL1wiLCBcImljb25cIjogXCJmYSBmYS1sb2NrXCIsIFwib3JkZXJcIjogNCwgXCJ0aXRsZVwiOiBcIlx1Njc0M1x1OTY1MFwifV0sIFwiZmlyc3RfaWNvblwiOiBcImZhIGZhLWxvY2tcIiwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL2F1dGgvcGVybWlzc2lvbi9cIiwgXCJ0aXRsZVwiOiBcIlx1OGJhNFx1OGJjMVx1NTQ4Y1x1NjM4OFx1Njc0M1wifSwge1wibWVudXNcIjogW3tcInVybFwiOiBcIi94YWRtaW4vY291cnNlcy9jb3Vyc2UvXCIsIFwiaWNvblwiOiBudWxsLCBcIm9yZGVyXCI6IDEwLCBcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXCJ9LCB7XCJ1cmxcIjogXCIveGFkbWluL2NvdXJzZXMvYmFubmVyY291cnNlL1wiLCBcImljb25cIjogbnVsbCwgXCJvcmRlclwiOiAxMSwgXCJ0aXRsZVwiOiBcIlx1OGY2ZVx1NjRhZFx1OGJmZVx1N2EwYlwifSwge1widXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL2xlc3Nvbi9cIiwgXCJpY29uXCI6IG51bGwsIFwib3JkZXJcIjogMTIsIFwidGl0bGVcIjogXCJcdTdhZTBcdTgyODJcIn0sIHtcInVybFwiOiBcIi94YWRtaW4vY291cnNlcy92aWRlby9cIiwgXCJpY29uXCI6IG51bGwsIFwib3JkZXJcIjogMTMsIFwidGl0bGVcIjogXCJcdTg5YzZcdTk4OTFcIn0sIHtcInVybFwiOiBcIi94YWRtaW4vY291cnNlcy9jb3Vyc2VyZXNvdXJjZS9cIiwgXCJpY29uXCI6IG51bGwsIFwib3JkZXJcIjogMTQsIFwidGl0bGVcIjogXCJcdThiZmVcdTdhMGJcdThkNDRcdTZlOTBcIn1dLCBcImZpcnN0X3VybFwiOiBcIi94YWRtaW4vY291cnNlcy92aWRlby9cIiwgXCJ0aXRsZVwiOiBcIlx1OGJmZVx1N2EwYlx1N2JhMVx1NzQwNlwifV0ifQ==','2017-12-12 20:44:41.979894'), ('rdm53f1ftj9a6pqg59fgo4anzc2emgnl','OWI1NDMyNjM5YzhjYjlkYmZhN2EyMTBlN2RlODA1MjA0ZDJlNzU5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjU1ZjgyZGMwMjYyNzFlNWFhNWQwNWFmYzM1ZGUyMDJjOTQ3ZjU5ZjAiLCJfYXV0aF91c2VyX2lkIjoiNCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2017-11-09 18:58:30.680661'), ('s9s6lcpxdbg7rf2ial8ma4jem6j7bwff','MGM4MjA1YmEzYTE2OGFkODEwOTAxNjBiYWVmYTQ3NTYwNzg5ZWYxNDp7IkxJU1RfUVVFUlkiOltbInVzZXJzIiwidXNlcnByb2ZpbGUiXSwiIl0sIm5hdl9tZW51IjoiW3tcIm1lbnVzXCI6IFt7XCJ1cmxcIjogXCIveGFkbWluL3VzZXJzL3VzZXJwcm9maWxlL1wiLCBcIm9yZGVyXCI6IDMsIFwidGl0bGVcIjogXCJcdTc1MjhcdTYyMzdcdTRmZTFcdTYwNmZcIiwgXCJpY29uXCI6IFwiZmEgZmEtdXNlclwifSwge1widXJsXCI6IFwiL3hhZG1pbi91c2Vycy9lbWFpbHZlcmlmeXJlY29yZC9cIiwgXCJvcmRlclwiOiA1LCBcInRpdGxlXCI6IFwiXHU5MGFlXHU3YmIxXHU5YThjXHU4YmMxXHU3ODAxXCIsIFwiaWNvblwiOiBcImZhIGZhLXVzZXItY2lyY2xlXCJ9LCB7XCJ1cmxcIjogXCIveGFkbWluL3VzZXJzL2Jhbm5lci9cIiwgXCJvcmRlclwiOiA2LCBcInRpdGxlXCI6IFwiXHU4ZjZlXHU2NGFkXHU1NmZlXCIsIFwiaWNvblwiOiBcImZhIGZhLWJsaW5kXCJ9XSwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL3VzZXJzL2Jhbm5lci9cIiwgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NGZlMVx1NjA2ZlwiLCBcImZpcnN0X2ljb25cIjogXCJmYSBmYS1ibGluZFwifSwge1wibWVudXNcIjogW3tcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJhc2svXCIsIFwib3JkZXJcIjogMTUsIFwidGl0bGVcIjogXCJcdTc1MjhcdTYyMzdcdTU0YThcdThiZTJcIiwgXCJpY29uXCI6IG51bGx9LCB7XCJ1cmxcIjogXCIveGFkbWluL29wZXJhdGlvbi9jb3Vyc2Vjb21tZW50cy9cIiwgXCJvcmRlclwiOiAxNiwgXCJ0aXRsZVwiOiBcIlx1OGJmZVx1N2EwYlx1OGJjNFx1OGJiYVwiLCBcImljb25cIjogbnVsbH0sIHtcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJmYXZvcml0ZS9cIiwgXCJvcmRlclwiOiAxNywgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NjUzNlx1ODVjZlwiLCBcImljb25cIjogbnVsbH0sIHtcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJtZXNzYWdlL1wiLCBcIm9yZGVyXCI6IDE4LCBcInRpdGxlXCI6IFwiXHU3NTI4XHU2MjM3XHU2ZDg4XHU2MDZmXCIsIFwiaWNvblwiOiBudWxsfSwge1widXJsXCI6IFwiL3hhZG1pbi9vcGVyYXRpb24vdXNlcmNvdXJzZS9cIiwgXCJvcmRlclwiOiAxOSwgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1OGJmZVx1N2EwYlwiLCBcImljb25cIjogbnVsbH1dLCBcImZpcnN0X3VybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJhc2svXCIsIFwidGl0bGVcIjogXCJcdTc1MjhcdTYyMzdcdTY0Y2RcdTRmNWNcIn0sIHtcIm1lbnVzXCI6IFt7XCJ1cmxcIjogXCIveGFkbWluL3hhZG1pbi9sb2cvXCIsIFwib3JkZXJcIjogMjEsIFwidGl0bGVcIjogXCJcdTY1ZTVcdTVmZDdcdThiYjBcdTVmNTVcIiwgXCJpY29uXCI6IFwiZmEgZmEtY29nXCJ9XSwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL3hhZG1pbi9sb2cvXCIsIFwidGl0bGVcIjogXCJcdTdiYTFcdTc0MDZcIiwgXCJmaXJzdF9pY29uXCI6IFwiZmEgZmEtY29nXCJ9LCB7XCJtZW51c1wiOiBbe1widXJsXCI6IFwiL3hhZG1pbi9vcmdhbml6YXRpb24vY2l0eWRpdHkvXCIsIFwib3JkZXJcIjogNywgXCJ0aXRsZVwiOiBcIlx1NTdjZVx1NWUwMlwiLCBcImljb25cIjogbnVsbH0sIHtcInVybFwiOiBcIi94YWRtaW4vb3JnYW5pemF0aW9uL2NvdXJzZW9yZy9cIiwgXCJvcmRlclwiOiA4LCBcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXHU2NzNhXHU2Nzg0XCIsIFwiaWNvblwiOiBudWxsfSwge1widXJsXCI6IFwiL3hhZG1pbi9vcmdhbml6YXRpb24vdGVhY2hlci9cIiwgXCJvcmRlclwiOiA5LCBcInRpdGxlXCI6IFwiXHU2NTU5XHU1ZTA4XCIsIFwiaWNvblwiOiBudWxsfV0sIFwiZmlyc3RfdXJsXCI6IFwiL3hhZG1pbi9vcmdhbml6YXRpb24vY291cnNlb3JnL1wiLCBcInRpdGxlXCI6IFwiXHU3YmExXHU3NDA2XHU2NzNhXHU2Nzg0XCJ9LCB7XCJtZW51c1wiOiBbe1widXJsXCI6IFwiL3hhZG1pbi9hdXRoL2dyb3VwL1wiLCBcIm9yZGVyXCI6IDIsIFwidGl0bGVcIjogXCJcdTdlYzRcIiwgXCJpY29uXCI6IFwiZmEgZmEtZ3JvdXBcIn0sIHtcInVybFwiOiBcIi94YWRtaW4vYXV0aC9wZXJtaXNzaW9uL1wiLCBcIm9yZGVyXCI6IDQsIFwidGl0bGVcIjogXCJcdTY3NDNcdTk2NTBcIiwgXCJpY29uXCI6IFwiZmEgZmEtbG9ja1wifV0sIFwiZmlyc3RfdXJsXCI6IFwiL3hhZG1pbi9hdXRoL2dyb3VwL1wiLCBcInRpdGxlXCI6IFwiXHU4YmE0XHU4YmMxXHU1NDhjXHU2Mzg4XHU2NzQzXCIsIFwiZmlyc3RfaWNvblwiOiBcImZhIGZhLWdyb3VwXCJ9LCB7XCJtZW51c1wiOiBbe1widXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL2NvdXJzZS9cIiwgXCJvcmRlclwiOiAxMCwgXCJ0aXRsZVwiOiBcIlx1OGJmZVx1N2EwYlwiLCBcImljb25cIjogbnVsbH0sIHtcInVybFwiOiBcIi94YWRtaW4vY291cnNlcy9iYW5uZXJjb3Vyc2UvXCIsIFwib3JkZXJcIjogMTEsIFwidGl0bGVcIjogXCJcdThmNmVcdTY0YWRcdThiZmVcdTdhMGJcIiwgXCJpY29uXCI6IG51bGx9LCB7XCJ1cmxcIjogXCIveGFkbWluL2NvdXJzZXMvbGVzc29uL1wiLCBcIm9yZGVyXCI6IDEyLCBcInRpdGxlXCI6IFwiXHU3YWUwXHU4MjgyXCIsIFwiaWNvblwiOiBudWxsfSwge1widXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL3ZpZGVvL1wiLCBcIm9yZGVyXCI6IDEzLCBcInRpdGxlXCI6IFwiXHU4OWM2XHU5ODkxXCIsIFwiaWNvblwiOiBudWxsfSwge1widXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL2NvdXJzZXJlc291cmNlL1wiLCBcIm9yZGVyXCI6IDE0LCBcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXHU4ZDQ0XHU2ZTkwXCIsIFwiaWNvblwiOiBudWxsfV0sIFwiZmlyc3RfdXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL2NvdXJzZS9cIiwgXCJ0aXRsZVwiOiBcIlx1OGJmZVx1N2EwYlx1N2JhMVx1NzQwNlwifV0iLCJfYXV0aF91c2VyX2hhc2giOiIwOTk4MDIyYzljOTY0MjBjNGMyMWJmMzBlNGMzY2ViNjEzMTdiMWMxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSJ9','2017-12-14 20:09:39.982610'), ('szve8vnoiw9iwypvci71rkrr3hle2y36','NDExNWU0MWNkYTFkNWU2NTU5ZTQ1MDNjZjQ4NDM5YTMwOGJhMDQ1ODp7Im5hdl9tZW51IjoiW3tcInRpdGxlXCI6IFwiXHU3NTI4XHU2MjM3XHU0ZmUxXHU2MDZmXCIsIFwibWVudXNcIjogW3tcIm9yZGVyXCI6IDMsIFwidGl0bGVcIjogXCJcdTc1MjhcdTYyMzdcdTRmZTFcdTYwNmZcIiwgXCJ1cmxcIjogXCIveGFkbWluL3VzZXJzL3VzZXJwcm9maWxlL1wiLCBcImljb25cIjogXCJmYSBmYS11c2VyXCJ9LCB7XCJvcmRlclwiOiA1LCBcInRpdGxlXCI6IFwiXHU5MGFlXHU3YmIxXHU5YThjXHU4YmMxXHU3ODAxXCIsIFwidXJsXCI6IFwiL3hhZG1pbi91c2Vycy9lbWFpbHZlcmlmeXJlY29yZC9cIiwgXCJpY29uXCI6IFwiZmEgZmEtdXNlci1jaXJjbGVcIn0sIHtcIm9yZGVyXCI6IDYsIFwidGl0bGVcIjogXCJcdThmNmVcdTY0YWRcdTU2ZmVcIiwgXCJ1cmxcIjogXCIveGFkbWluL3VzZXJzL2Jhbm5lci9cIiwgXCJpY29uXCI6IFwiZmEgZmEtYmxpbmRcIn1dLCBcImZpcnN0X3VybFwiOiBcIi94YWRtaW4vdXNlcnMvZW1haWx2ZXJpZnlyZWNvcmQvXCIsIFwiZmlyc3RfaWNvblwiOiBcImZhIGZhLXVzZXItY2lyY2xlXCJ9LCB7XCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NjRjZFx1NGY1Y1wiLCBcIm1lbnVzXCI6IFt7XCJvcmRlclwiOiAxNSwgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NTRhOFx1OGJlMlwiLCBcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJhc2svXCIsIFwiaWNvblwiOiBudWxsfSwge1wib3JkZXJcIjogMTYsIFwidGl0bGVcIjogXCJcdThiZmVcdTdhMGJcdThiYzRcdThiYmFcIiwgXCJ1cmxcIjogXCIveGFkbWluL29wZXJhdGlvbi9jb3Vyc2Vjb21tZW50cy9cIiwgXCJpY29uXCI6IG51bGx9LCB7XCJvcmRlclwiOiAxNywgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NjUzNlx1ODVjZlwiLCBcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJmYXZvcml0ZS9cIiwgXCJpY29uXCI6IG51bGx9LCB7XCJvcmRlclwiOiAxOCwgXCJ0aXRsZVwiOiBcIlx1NzUyOFx1NjIzN1x1NmQ4OFx1NjA2ZlwiLCBcInVybFwiOiBcIi94YWRtaW4vb3BlcmF0aW9uL3VzZXJtZXNzYWdlL1wiLCBcImljb25cIjogbnVsbH0sIHtcIm9yZGVyXCI6IDE5LCBcInRpdGxlXCI6IFwiXHU3NTI4XHU2MjM3XHU4YmZlXHU3YTBiXCIsIFwidXJsXCI6IFwiL3hhZG1pbi9vcGVyYXRpb24vdXNlcmNvdXJzZS9cIiwgXCJpY29uXCI6IG51bGx9XSwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL29wZXJhdGlvbi9jb3Vyc2Vjb21tZW50cy9cIn0sIHtcInRpdGxlXCI6IFwiXHU3YmExXHU3NDA2XCIsIFwibWVudXNcIjogW3tcIm9yZGVyXCI6IDIxLCBcInRpdGxlXCI6IFwiXHU2NWU1XHU1ZmQ3XHU4YmIwXHU1ZjU1XCIsIFwidXJsXCI6IFwiL3hhZG1pbi94YWRtaW4vbG9nL1wiLCBcImljb25cIjogXCJmYSBmYS1jb2dcIn1dLCBcImZpcnN0X3VybFwiOiBcIi94YWRtaW4veGFkbWluL2xvZy9cIiwgXCJmaXJzdF9pY29uXCI6IFwiZmEgZmEtY29nXCJ9LCB7XCJ0aXRsZVwiOiBcIlx1N2JhMVx1NzQwNlx1NjczYVx1Njc4NFwiLCBcIm1lbnVzXCI6IFt7XCJvcmRlclwiOiA3LCBcInRpdGxlXCI6IFwiXHU1N2NlXHU1ZTAyXCIsIFwidXJsXCI6IFwiL3hhZG1pbi9vcmdhbml6YXRpb24vY2l0eWRpdHkvXCIsIFwiaWNvblwiOiBudWxsfSwge1wib3JkZXJcIjogOCwgXCJ0aXRsZVwiOiBcIlx1OGJmZVx1N2EwYlx1NjczYVx1Njc4NFwiLCBcInVybFwiOiBcIi94YWRtaW4vb3JnYW5pemF0aW9uL2NvdXJzZW9yZy9cIiwgXCJpY29uXCI6IG51bGx9LCB7XCJvcmRlclwiOiA5LCBcInRpdGxlXCI6IFwiXHU2NTU5XHU1ZTA4XCIsIFwidXJsXCI6IFwiL3hhZG1pbi9vcmdhbml6YXRpb24vdGVhY2hlci9cIiwgXCJpY29uXCI6IG51bGx9XSwgXCJmaXJzdF91cmxcIjogXCIveGFkbWluL29yZ2FuaXphdGlvbi90ZWFjaGVyL1wifSwge1widGl0bGVcIjogXCJcdThiYTRcdThiYzFcdTU0OGNcdTYzODhcdTY3NDNcIiwgXCJtZW51c1wiOiBbe1wib3JkZXJcIjogMiwgXCJ0aXRsZVwiOiBcIlx1N2VjNFwiLCBcInVybFwiOiBcIi94YWRtaW4vYXV0aC9ncm91cC9cIiwgXCJpY29uXCI6IFwiZmEgZmEtZ3JvdXBcIn0sIHtcIm9yZGVyXCI6IDQsIFwidGl0bGVcIjogXCJcdTY3NDNcdTk2NTBcIiwgXCJ1cmxcIjogXCIveGFkbWluL2F1dGgvcGVybWlzc2lvbi9cIiwgXCJpY29uXCI6IFwiZmEgZmEtbG9ja1wifV0sIFwiZmlyc3RfdXJsXCI6IFwiL3hhZG1pbi9hdXRoL3Blcm1pc3Npb24vXCIsIFwiZmlyc3RfaWNvblwiOiBcImZhIGZhLWxvY2tcIn0sIHtcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXHU3YmExXHU3NDA2XCIsIFwibWVudXNcIjogW3tcIm9yZGVyXCI6IDEwLCBcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXCIsIFwidXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL2NvdXJzZS9cIiwgXCJpY29uXCI6IG51bGx9LCB7XCJvcmRlclwiOiAxMSwgXCJ0aXRsZVwiOiBcIlx1OGY2ZVx1NjRhZFx1OGJmZVx1N2EwYlwiLCBcInVybFwiOiBcIi94YWRtaW4vY291cnNlcy9iYW5uZXJjb3Vyc2UvXCIsIFwiaWNvblwiOiBudWxsfSwge1wib3JkZXJcIjogMTIsIFwidGl0bGVcIjogXCJcdTdhZTBcdTgyODJcIiwgXCJ1cmxcIjogXCIveGFkbWluL2NvdXJzZXMvbGVzc29uL1wiLCBcImljb25cIjogbnVsbH0sIHtcIm9yZGVyXCI6IDEzLCBcInRpdGxlXCI6IFwiXHU4OWM2XHU5ODkxXCIsIFwidXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL3ZpZGVvL1wiLCBcImljb25cIjogbnVsbH0sIHtcIm9yZGVyXCI6IDE0LCBcInRpdGxlXCI6IFwiXHU4YmZlXHU3YTBiXHU4ZDQ0XHU2ZTkwXCIsIFwidXJsXCI6IFwiL3hhZG1pbi9jb3Vyc2VzL2NvdXJzZXJlc291cmNlL1wiLCBcImljb25cIjogbnVsbH1dLCBcImZpcnN0X3VybFwiOiBcIi94YWRtaW4vY291cnNlcy9sZXNzb24vXCJ9XSIsIl9hdXRoX3VzZXJfaGFzaCI6IjA5OTgwMjJjOWM5NjQyMGM0YzIxYmYzMGU0YzNjZWI2MTMxN2IxYzEiLCJMSVNUX1FVRVJZIjpbWyJvcGVyYXRpb24iLCJ1c2VyY291cnNlIl0sIiJdLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2017-12-13 14:50:14.678282'), ('tnj25by83f07rjhbnwm5wfuqrzzxi7uz','MmIwMGEyZjk0MjJkNTk4NmM1MDhmNzc0NzYxNWU2ODY2MmQ0NGU4Mzp7Il9hdXRoX3VzZXJfaGFzaCI6IjA5OTgwMjJjOWM5NjQyMGM0YzIxYmYzMGU0YzNjZWI2MTMxN2IxYzEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2017-11-28 10:13:46.937448');
INSERT INTO `operation_coursecomments` VALUES ('1','老师讲得真不错！！','2017-11-04 21:02:10.458394','1','1'), ('2','划水划水···','2017-11-04 21:02:53.573724','1','1'), ('3','年薪30W？？？','2017-11-04 21:05:39.013518','1','1');
INSERT INTO `operation_userask` VALUES ('7','sadk','13244237213','math','2017-10-30 21:38:30.229733'), ('8','tayle','13979645874','english','2017-10-31 18:50:45.232977'), ('31','阿萨德','13143434343','阿斯顿','2017-10-31 19:08:37.678944'), ('32','sad','13244237777','12','2017-11-01 19:50:56.850628'), ('33','撒的','13111111111','sadasd','2017-11-01 19:51:11.961872'), ('34','asd','13111112222','asdasd','2017-11-01 19:52:11.620673');
INSERT INTO `operation_usercourse` VALUES ('1','2017-11-02 21:33:00.000000','1','1'), ('5','2017-11-02 21:40:00.000000','5','5'), ('6','2017-11-05 18:53:01.801658','1','5'), ('7','2017-11-05 19:24:18.708048','8','5'), ('8','2017-11-05 20:19:37.177081','8','1'), ('9','2017-11-14 10:14:36.571512','10','1'), ('10','2017-11-29 11:57:59.745012','9','1'), ('11','2017-11-29 11:59:24.882059','11','1'), ('12','2017-11-29 11:59:47.487785','7','1'), ('13','2017-11-29 14:05:38.493213','4','1');
INSERT INTO `operation_userfavorite` VALUES ('19','1','1','2017-11-04 20:01:49.150860','1'), ('28','1','3','2017-11-08 20:28:18.241984','1'), ('29','3','3','2017-11-19 21:04:23.177473','1'), ('30','12','1','2017-11-19 21:23:03.189117','1'), ('31','7','2','2017-11-19 21:25:54.740908','1'), ('36','3','2','2017-11-29 14:05:26.171509','1'), ('38','10','2','2017-11-29 14:12:39.775309','1');
INSERT INTO `operation_usermessage` VALUES ('1','1','欢迎注册','1','2017-11-20 20:32:00.000000');
INSERT INTO `organization_citydity` VALUES ('1','北京市','北京市','2017-10-28 20:03:00.000000'), ('2','上海市','上海市','2017-10-28 20:03:00.000000'), ('3','广州市','广州市','2017-10-28 20:09:00.000000'), ('4','深圳市','深圳市','2017-10-28 20:10:00.000000'), ('5','天津市','天津市','2017-10-28 20:10:00.000000');
INSERT INTO `organization_courseorg` VALUES ('3','加州大学伯里克利学院','慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n       慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。','8','1','org/2017/11/Berkeley.png','北京市','2017-10-28 20:31:00.000000','1','pxjg','0','0','全国知名'), ('4','复旦大学','复旦大学（Fudan University），简称“复旦”，位于中国上海，由中华人民共和国教育部直属，中央直管副部级建制，位列211工程、985工程，入选双一流、“珠峰计划”、“111计划”、“2011计划”、“卓越医生教育培养计划”，为“九校联盟”成员、中国大学校长联谊会成员、东亚研究型大学协会成员、环太平洋大学协会成员、21世纪大学协会成员，是一所综合性研究型的全国重点大学','1','0','org/2017/11/Fudan.png','北京市','2017-10-28 20:33:00.000000','1','pxjg','0','0','全国知名'), ('5','斯坦福大学','斯坦福大学（Stanford University），全名小利兰·斯坦福大学（Leland Stanford Junior University），简称“斯坦福（Stanford）”，位于美国加州旧金山湾区南部的帕罗奥多市（Palo Alto）境内[1]  ，临近世界著名高科技园区硅谷，是世界著名私立研究型大学[1]  。斯坦福大学占地约33平方公里（8180英亩）[2]  ，是美国占地面积第六大的大学','15','0','org/2017/11/Stanford.png','上海市','2017-10-28 20:33:00.000000','2','gx','0','0','全国知名'), ('6','台湾清华大学','慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n       慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。','0','0','org/2017/11/sharecourse.png','深圳市','2017-10-28 20:34:00.000000','1','gr','0','0','全国知名'), ('7','eDX国际','edX是麻省理工和哈佛大学于2012年4月联手创建的大规模开放在线课堂平台。','6565','0','org/2017/11/hezuo_1.jpg','天津市','2017-10-28 20:34:00.000000','1','gr','0','0','全国知名'), ('8','慕课网','慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n       慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。','0','0','org/2017/10/imooc_klgAUn5.png','广州市','2017-10-28 20:34:00.000000','4','pxjg','0','0','全国知名'), ('9','北京大学','慕课网是垂直的互联网IT技能免费学习网站。以独家视频教程、在线编程工具、学习计划、问答社区为核心特色。在这里，你可以找到最好的互联网技术牛人，也可以通过免费的在线公开视频课程学习国内领先的互联网IT技术。\r\n       慕课网课程涵盖前端开发、PHP、Html5、Android、iOS、Swift等IT前沿技术语言，包括基础课程、实用案例、高级分享三大类型，适合不同阶段的学习人群。以纯干货、短视频的形式为平台特点，为在校学生、职场白领提供了一个迅速提升技能、共同分享进步的学习平台。','0','0','org/2017/11/beida.jpg','北京市','2017-10-28 20:35:00.000000','1','pxjg','0','0','全国知名'), ('10','北京大学','北京大学，简称“北大”，诞生于1898年，初名京师大学堂，是中国近代第一所国立大学，也是最早以“大学”之名创办的学校，其成立标志着中国近代高等教育的开端。北大是中国近代以来唯一以国家最高学府身份创立的学校，最初也是国家最高教育行政机关，行使教育部职能，统管全国教育。北大催生了中国最早的现代学制，开创了中国最早的文科、理科、社科、农科、医科等大学学科，是近代以来中国高等教育的奠基者','0','1','org/2017/10/bjdx.jpg','广州市','2017-10-28 20:36:00.000000','2','gx','0','0','全国知名'), ('11','UNESCO','1945年11月1日-16日，二战刚刚结束，根据盟国教育部长会议的提议，在伦敦举行了旨在成立一个教育及文化组织的联合国会议（ECO/CONF）。约四十个国家的代表出席了这次会议。在饱经战争苦难的两个国家——法国和英国的推动下，会议代表决定成立一个以建立真正和平文化为宗旨的组织。按照他们的设想，这个新的组织应建立“人类智力上和道义上的团结”，从而防止爆发新的世界大战。','56','0','org/2017/11/hezuo_2.jpg','深圳市','2017-10-28 20:36:00.000000','2','gx','0','0','全国知名'), ('12','网络工程','培养目标：旨在培养德、智、体全面发展，具有良好的科学素养，较系统地掌握网络工程专业所必须的基本理论、基本知识和基本技能与方法，能在企事业、技术和行政管理部门从事计算机网络系统规划、设计、实施、应用与管理的应用型高级专门技术人才。 修业年限：4年 授予学位：工学学士 主干课程：程序设计基础、面向对象程序设计、数据结构、计算机组成原理、操作系统原理、数据库原理及应用、计算机网络基础、通信原理基础、网络协议、网络规划与设计、网络互联技术、网络操作系统。 就业方向 ：在企事业、技术和行政管理部门从事计算机网络系统规划、设计、实施、应用与管理工作。','0','0','org/2017/11/beihua_kgOag7C.jpg','上海市','2017-10-28 20:37:00.000000','3','gx','0','123','全国知名'), ('13','国际mooc','MOOC全称大规模开放式在线课程（Massive Open Online Course），是当前最新、最潮的在线学习形式，让你足不出户听遍名校人气课程，学习专业知识，提高职业技能。\r\n果壳网MOOC学院是全球最大的中文MOOC学习者社区，带你发现全球在线好课','0','0','org/2017/11/mooc.jpg','上海','2017-11-28 23:04:00.000000','2','gr','0','0','全国知名');
INSERT INTO `organization_teacher` VALUES ('1','赵丽颖','8','京东','歌手','cute','464376','2017-10-31 19:17:00.000000','3','0','teacher/2017/m/u27530878023883867419fm27gp0.jpg','18'), ('2','Jimly','5','腾讯','高级架构师','666','24324','2017-10-31 19:18:00.000000','11','0','teacher/2017/m/u39017916941271045882fm27gp0.jpg','18'), ('3','kerberos','3','阿里巴巴','高级数据分析师','幽默','12335','2017-11-01 16:10:00.000000','7','3242','teacher/2017/m/60ca4afc4dffe821400x400_big.jpg','18'), ('4','huang_xiaohen','3','极客学院','极客学院签约布道师','流畅','22','2017-11-29 11:11:00.000000','6','0','teacher/2017/m/552505a98defe.jpg','23'), ('5','李红果','213','北华大学','老师','简洁而清晰','5','2017-11-29 11:13:00.000000','10','22','teacher/2017/m/u322021610262555773fm27gp0.jpg','33'), ('6','韦玮','12','极客学院','极客学院签约布道师','简洁','1222','2017-11-29 11:16:00.000000','12','2','teacher/2017/m/07eybedh65tzl.jpeg','43'), ('7','dragonszy','12','京东','极客学院签约布道师','简洁','12','2017-11-29 11:22:00.000000','5','0','teacher/2017/m/55ac55d962768.jpg','18');
INSERT INTO `users_banner` VALUES ('1','艺术经典','banaer/201711/lun3_9ErbOcy.jpg','https://baike.baidu.com/item/%E7%8C%8E%E5%9C%BA/15388743?fr=aladdin','1','2017-11-23 16:01:00.000000'), ('2','VR','banaer/201711/lun5.jpg','https://baike.baidu.com/item/%E7%8C%8E%E5%9C%BA/15388743?fr=aladdin','2','2017-11-23 16:03:00.000000'), ('3','信号与系统','banaer/201711/lun4.jpg','http://www.iqiyi.com/a_19rrhbeyx9.html?vfm=2008_aldbd','3','2017-11-23 16:05:00.000000'), ('4','废墟上的崛起','banaer/201711/lun2.jpg','http://www.kumi.cn/donghua/57370.html','4','2017-11-23 16:07:00.000000'), ('5','创业导引','banaer/201711/lun1.jpg','http://www.iqiyi.com/a_19rrhb5shh.html?vfm=2008_aldbd','5','2017-11-23 16:10:00.000000');
INSERT INTO `users_emailverifyrecord` VALUES ('1','admin','asdssd@qq.com','register','2017-10-08 21:11:00.000000'), ('2','beihua','asdsd@162.com','register','2017-10-08 21:26:00.000000'), ('3','d58ihsn89pvfvi5f','1307978xxxx@sina.cn','register','2017-10-24 18:07:32.630919'), ('4','9v4anfviussi6dhj','1307978xxxx@sina.cn','register','2017-10-24 18:12:33.595562'), ('5','sa98jggcvspy9p95','1307978xxxx@sina.cn','forget','2017-10-24 20:10:25.828569'), ('6','ia4sp1ahjhya4ncs','212@163.com','update_email','2017-11-14 09:43:15.610642'), ('7','59vfhi4ssusgigju','1307978xxxx@sina.cn','update_email','2017-11-14 15:59:32.355874'), ('8','d988f989yffipfij','1307978xxxx@sina.cn','update_email','2017-11-14 16:02:22.147078'), ('9','g919','1307978xxxx@sina.cn','update_email','2017-11-14 16:04:08.853588');
INSERT INTO `users_userprofile` VALUES ('1','pbkdf2_sha256$24000$0ab6NwEgEXTi$xaFJ2HWaQmjHUPYb6XJVm8wUeV9MqdwBzaT9oBvDE6o=','2017-11-30 20:08:27.298037','1','admin','','','1307978xxxx@sina.cn','1','1','2017-10-08 11:56:00.000000','kerberos','2017-11-07','female','jilin','13244444444','image/2017/11/ly_NlUJKtk.jpg'), ('4','pbkdf2_sha256$24000$aaYoZIJvjU50$+/5xangIzfsmtdIorKLhSNGukMHV6/SxQi8rLCePi30=','2017-10-26 18:58:30.575894','0','1307978xxxx@sina.cn','','','1307978970@sina.cn','1','1','2017-10-24 18:12:33.497478','',NULL,'female','',NULL,'teacher/2017/m/default_middile_1.png'), ('5','pbkdf2_sha256$24000$ekB9Oa16wpv6$uTFPObGYcSshxTqFSh3+BxQw6ukYcheoL9oFEejkixY=','2017-11-14 09:18:03.134444','0','kamiy','','','','1','1','2017-11-02 21:33:02.238763','',NULL,'female','',NULL,'teacher/2017/m/default_middile_1.png'), ('6','pbkdf2_sha256$24000$6jSJ8unOS0FD$o9rG3fqlhJXHEF4DF+29pTVn1Zskczj8mtJT5U0tLZI=',NULL,'1','zhj','','','zhj123@qq.com','1','1','2017-11-29 15:03:42.946513','',NULL,'female','',NULL,'image/default.png');
INSERT INTO `xadmin_log` VALUES ('1','2017-10-08 21:12:18.452401','127.0.0.1','1','admin(asdssd@qq.com)','create','已添加。','8','1'), ('2','2017-10-08 21:26:35.310508','127.0.0.1','2','beihua(asdsd@162.com)','create','已添加。','8','1'), ('3','2017-10-28 20:03:40.338524','127.0.0.1','1','CityDity object','create','已添加。','10','1'), ('4','2017-10-28 20:09:40.978585','127.0.0.1','2','CityDity object','create','已添加。','10','1'), ('5','2017-10-28 20:09:52.337141','127.0.0.1','3','CityDity object','create','已添加。','10','1'), ('6','2017-10-28 20:10:10.232375','127.0.0.1','4','CityDity object','create','已添加。','10','1'), ('7','2017-10-28 20:10:31.051267','127.0.0.1','5','CityDity object','create','已添加。','10','1'), ('8','2017-10-28 20:24:57.954924','127.0.0.1','1','CourseOrg object','create','已添加。','11','1'), ('9','2017-10-28 20:27:54.010821','127.0.0.1',NULL,'','delete','批量删除 1 个 课程机构',NULL,'1'), ('10','2017-10-28 20:29:30.388098','127.0.0.1','2','慕课网','create','已添加。','11','1'), ('11','2017-10-28 20:31:22.385836','127.0.0.1',NULL,'','delete','批量删除 1 个 课程机构',NULL,'1'), ('12','2017-10-28 20:31:49.505109','127.0.0.1','3','慕课网','create','已添加。','11','1'), ('13','2017-10-28 20:33:54.353049','127.0.0.1','4','慕课网1','create','已添加。','11','1'), ('14','2017-10-28 20:34:15.040684','127.0.0.1','5','慕课网2','create','已添加。','11','1'), ('15','2017-10-28 20:34:34.419273','127.0.0.1','6','慕课网3','create','已添加。','11','1'), ('16','2017-10-28 20:34:55.453292','127.0.0.1','7','慕课网4','create','已添加。','11','1'), ('17','2017-10-28 20:35:17.826195','127.0.0.1','8','慕课网5','create','已添加。','11','1'), ('18','2017-10-28 20:35:53.497028','127.0.0.1','9','慕课网6','create','已添加。','11','1'), ('19','2017-10-28 20:36:40.130284','127.0.0.1','10','慕课网556','create','已添加。','11','1'), ('20','2017-10-28 20:37:03.737846','127.0.0.1','11','慕课网46','create','已添加。','11','1'), ('21','2017-10-28 20:37:23.099493','127.0.0.1','12','慕课网464','create','已添加。','11','1'), ('22','2017-10-31 19:18:37.255535','127.0.0.1','1','Teacher object','create','已添加。','12','1'), ('23','2017-10-31 19:19:36.401190','127.0.0.1','2','kerberos','create','已添加。','12','1'), ('24','2017-10-31 19:38:50.397003','127.0.0.1','1','django','create','已添加。','13','1'), ('25','2017-10-31 20:59:05.285984','127.0.0.1','2','kerberos','change','已修改 image 。','12','1'), ('26','2017-11-01 14:34:25.835691','127.0.0.1','2','kerberos','change','已修改 org 。','12','1'), ('27','2017-11-01 16:10:52.430947','127.0.0.1','3','kerberos','create','已添加。','12','1'), ('28','2017-11-01 20:25:54.628509','127.0.0.1','2','go语言入门','create','已添加。','13','1'), ('29','2017-11-01 20:26:30.044288','127.0.0.1','3','c++入门到放弃','create','已添加。','13','1'), ('30','2017-11-01 20:26:55.421670','127.0.0.1','4','python异常处理','create','已添加。','13','1'), ('31','2017-11-01 20:27:50.156514','127.0.0.1','5','python面向对象','create','已添加。','13','1'), ('32','2017-11-01 20:28:28.456009','127.0.0.1','6','python文件处理','create','已添加。','13','1'), ('33','2017-11-01 20:28:56.100452','127.0.0.1','7','mysql高级教程','create','已添加。','13','1'), ('34','2017-11-01 20:30:12.677774','127.0.0.1','8','java21天精通','create','已添加。','13','1'), ('35','2017-11-01 20:30:54.996113','127.0.0.1','9','python网络编程','create','已添加。','13','1'), ('36','2017-11-01 20:31:30.744820','127.0.0.1','10','python高级编程','create','已添加。','13','1'), ('37','2017-11-01 20:32:08.632112','127.0.0.1','11','python数据分析实战','create','已添加。','13','1'), ('38','2017-11-01 20:39:58.029564','127.0.0.1','11','python数据分析实战','change','已修改 course_org 。','13','1'), ('39','2017-11-01 20:40:05.984591','127.0.0.1','9','python网络编程','change','已修改 course_org 。','13','1'), ('40','2017-11-01 20:40:11.360615','127.0.0.1','6','python文件处理','change','已修改 course_org 。','13','1'), ('41','2017-11-01 20:40:21.917364','127.0.0.1','3','c++入门到放弃','change','已修改 course_org 。','13','1'), ('42','2017-11-02 21:33:02.388869','127.0.0.1','5','kamiy','create','已添加。','7','1'), ('43','2017-11-02 21:34:09.142832','127.0.0.1','1','UserCourse object','create','已添加。','21','1'), ('44','2017-11-02 21:40:08.805570','127.0.0.1','5','UserCourse object','create','已添加。','21','1'), ('45','2017-11-03 19:06:22.745597','127.0.0.1','11','python数据分析实战','change','已修改 tag 。','13','1'), ('46','2017-11-03 19:06:34.383004','127.0.0.1','5','python面向对象','change','已修改 tag 。','13','1'), ('47','2017-11-03 19:07:23.443706','127.0.0.1','11','python数据分析实战','change','没有字段被修改。','13','1'), ('48','2017-11-03 19:11:21.507480','127.0.0.1','11','python数据分析实战','change','没有字段被修改。','13','1'), ('49','2017-11-03 19:12:32.700013','127.0.0.1','1','django','change','已修改 tag 。','13','1'), ('50','2017-11-03 19:12:44.023333','127.0.0.1','4','python异常处理','change','已修改 tag 。','13','1'), ('51','2017-11-03 20:07:00.426060','127.0.0.1','1','Lesson object','create','已添加。','14','1'), ('52','2017-11-03 20:07:18.338044','127.0.0.1','1','Lesson object','change','已修改 course 。','14','1'), ('53','2017-11-03 20:08:39.476369','127.0.0.1','2','Lesson object','create','已添加。','14','1'), ('54','2017-11-03 20:15:23.322334','127.0.0.1','1','1.1 hellowird','create','已添加。','15','1'), ('55','2017-11-03 20:16:30.008843','127.0.0.1','2','1.2 基础概念','create','已添加。','15','1'), ('56','2017-11-03 20:19:24.898987','127.0.0.1','3','2.1 高级编程','create','已添加。','15','1'), ('57','2017-11-03 20:19:53.333749','127.0.0.1','4','2.2 高级处理技巧','create','已添加。','15','1'), ('58','2017-11-03 20:33:34.741136','127.0.0.1','1','CourseResource object','create','已添加。','16','1'), ('59','2017-11-03 20:58:20.067807','127.0.0.1','1','django','change','已修改 teacher 。','13','1'), ('60','2017-11-03 21:05:48.721646','127.0.0.1','1','django','change','已修改 youneed_know 和 teacher_tell 。','13','1'), ('61','2017-11-05 20:55:15.977256','127.0.0.1','1','1.1 如何使用github[tortoisegit]','change','已修改 name 和 url 。','15','1'), ('62','2017-11-08 18:59:25.064434','127.0.0.1','3','kerberos','change','已修改 work_company，work_postion 和 points 。','12','1'), ('63','2017-11-08 19:00:04.796462','127.0.0.1','2','Jimly','change','已修改 name，work_company，work_postion 和 points 。','12','1'), ('64','2017-11-08 19:00:16.558623','127.0.0.1','1','赵丽颖','change','已修改 work_company 。','12','1'), ('65','2017-11-08 20:28:28.231641','127.0.0.1','1','赵丽颖','change','没有字段被修改。','12','1'), ('66','2017-11-10 20:54:08.642626','127.0.0.1','1','admin','change','已修改 nick_name 和 address 。','7','1'), ('67','2017-11-20 20:32:17.318978','127.0.0.1','1','UserMessage object','create','已添加。','20','1'), ('68','2017-11-20 20:35:30.568457','127.0.0.1','1','UserMessage object','change','已修改 user 。','20','1'), ('69','2017-11-23 16:03:21.504738','127.0.0.1','1','Banner object','create','已添加。','9','1'), ('70','2017-11-23 16:04:48.047362','127.0.0.1','2','Banner object','create','已添加。','9','1'), ('71','2017-11-23 16:05:37.461581','127.0.0.1','1','Banner object','change','已修改 image 。','9','1'), ('72','2017-11-23 16:06:31.645101','127.0.0.1','3','Banner object','create','已添加。','9','1'), ('73','2017-11-23 16:09:59.360846','127.0.0.1','4','Banner object','create','已添加。','9','1'), ('74','2017-11-23 16:10:28.964761','127.0.0.1','5','Banner object','create','已添加。','9','1'), ('75','2017-11-23 16:24:04.646697','127.0.0.1','1','Banner object','change','已修改 image 。','9','1'), ('76','2017-11-23 16:43:44.038125','127.0.0.1','1','Banner object','change','已修改 image 。','9','1'), ('77','2017-11-23 17:07:49.926861','127.0.0.1','5','Banner object','change','已修改 title 和 url 。','9','1'), ('78','2017-11-23 17:08:17.216643','127.0.0.1','4','Banner object','change','已修改 title 和 url 。','9','1'), ('79','2017-11-23 17:08:59.289601','127.0.0.1','3','Banner object','change','已修改 title，image 和 url 。','9','1'), ('80','2017-11-23 17:09:33.827192','127.0.0.1','2','Banner object','change','已修改 image 和 url 。','9','1'), ('81','2017-11-23 17:09:46.821481','127.0.0.1','2','Banner object','change','已修改 title 。','9','1'), ('82','2017-11-23 17:10:15.225407','127.0.0.1','1','Banner object','change','已修改 image 和 url 。','9','1'), ('83','2017-11-23 21:47:42.760374','127.0.0.1','1','django','change','已修改 is_banner 。','13','1'), ('84','2017-11-28 21:43:58.387196','127.0.0.1','5','Banner object','change','已修改 title 和 image 。','9','1'), ('85','2017-11-28 21:44:20.500902','127.0.0.1','4','Banner object','change','已修改 title 和 image 。','9','1'), ('86','2017-11-28 21:44:37.786210','127.0.0.1','3','Banner object','change','已修改 title 和 image 。','9','1'), ('87','2017-11-28 21:45:03.327104','127.0.0.1','2','Banner object','change','已修改 title 和 image 。','9','1'), ('88','2017-11-28 21:45:37.408528','127.0.0.1','1','Banner object','change','已修改 title 和 image 。','9','1'), ('89','2017-11-28 21:45:50.775170','127.0.0.1','3','Banner object','change','已修改 image 。','9','1'), ('90','2017-11-28 22:22:48.976879','127.0.0.1','12','网络工程1','change','已修改 name，desc，category，image 和 students 。','11','1'), ('91','2017-11-28 22:24:53.978362','127.0.0.1','11','网络工程2','change','已修改 name，desc，category 和 image 。','11','1'), ('92','2017-11-28 22:25:01.292851','127.0.0.1','12','网络工程1','change','已修改 image 。','11','1'), ('93','2017-11-28 22:25:19.523267','127.0.0.1','12','网络工程1','change','已修改 desc 。','11','1'), ('94','2017-11-28 22:25:31.096536','127.0.0.1','11','网络工程2','change','没有字段被修改。','11','1'), ('95','2017-11-28 22:25:38.169652','127.0.0.1','12','网络工程2','change','已修改 name 。','11','1'), ('96','2017-11-28 22:25:44.644236','127.0.0.1','12','网络工程','change','已修改 name 。','11','1'), ('97','2017-11-28 22:25:51.488930','127.0.0.1','11','网络工程','change','已修改 name 。','11','1'), ('98','2017-11-28 22:26:23.898962','127.0.0.1','10','北京大学','change','已修改 desc 。','11','1'), ('99','2017-11-28 22:27:22.669097','127.0.0.1','9','北京大学','change','已修改 name，image，address 和 city 。','11','1'), ('100','2017-11-28 22:27:37.274307','127.0.0.1','8','慕课网','change','已修改 name 和 category 。','11','1');
INSERT INTO `xadmin_log` VALUES ('101','2017-11-28 22:29:19.244597','127.0.0.1','7','Eric Grimson','change','已修改 name，desc，category 和 image 。','11','1'), ('102','2017-11-28 22:56:34.254089','127.0.0.1','6','台湾清华大学','change','已修改 name 和 image 。','11','1'), ('103','2017-11-28 22:58:14.006519','127.0.0.1','5','斯坦福大学','change','已修改 name，desc 和 image 。','11','1'), ('104','2017-11-28 22:59:49.632834','127.0.0.1','3','加州大学伯里克利学院','change','已修改 name 和 image 。','11','1'), ('105','2017-11-28 23:00:39.371138','127.0.0.1','4','复旦大学','change','已修改 name，desc 和 image 。','11','1'), ('106','2017-11-28 23:03:31.229306','127.0.0.1','11','UNESCO','change','已修改 name，desc 和 image 。','11','1'), ('107','2017-11-28 23:04:20.981017','127.0.0.1','7','eDX国际','change','已修改 name，desc 和 image 。','11','1'), ('108','2017-11-28 23:05:56.378163','127.0.0.1','13','国际mooc','create','已添加。','11','1'), ('109','2017-11-29 11:00:35.886864','127.0.0.1','4','无服务器架构系列讲解 - Lambda + API Gateway','change','已修改 name，detail，teacher，image，category，tag，youneed_know 和 teacher_tell 。','13','1'), ('110','2017-11-29 11:02:35.166218','127.0.0.1','9','微软人工智能 － 服务和API','change','已修改 name，desc，detail，teacher，image，category，tag，youneed_know 和 teacher_tell 。','13','1'), ('111','2017-11-29 11:05:06.990101','127.0.0.1','6','AWS云中的设计高可用架构-弹性和高可用的思考','change','已修改 name，desc，detail，teacher，image，category，tag，youneed_know 和 teacher_tell 。','13','1'), ('112','2017-11-29 11:08:09.766825','127.0.0.1','8','Tornado 开发--TCP 编程','change','已修改 course_org，name，desc，detail，is_banner，teacher，learn_times，image，category，tag，youneed_know 和 teacher_tell 。','13','1'), ('113','2017-11-29 11:13:10.581452','127.0.0.1','4','huang_xiaohen','create','已添加。','12','1'), ('114','2017-11-29 11:14:53.484184','127.0.0.1','5','李红果','create','已添加。','12','1'), ('115','2017-11-29 11:15:26.342196','127.0.0.1','1','赵丽颖','change','已修改 image 。','12','1'), ('116','2017-11-29 11:15:59.816947','127.0.0.1','2','Jimly','change','已修改 org 和 image 。','12','1'), ('117','2017-11-29 11:16:33.621030','127.0.0.1','3','kerberos','change','已修改 image 。','12','1'), ('118','2017-11-29 11:18:29.867126','127.0.0.1','6','韦玮','create','已添加。','12','1'), ('119','2017-11-29 11:24:25.030095','127.0.0.1','7','dragonszy','create','已添加。','12','1'), ('120','2017-11-29 11:49:45.352116','127.0.0.1','1','第一章 基础语法','change','已修改 course 。','14','1'), ('121','2017-11-29 11:49:52.523685','127.0.0.1','2','第二章 进阶开发','change','已修改 course 。','14','1'), ('122','2017-11-29 11:50:14.223400','127.0.0.1','2','第二章 进阶开发','change','已修改 course 。','14','1'), ('123','2017-11-29 11:50:20.935462','127.0.0.1','1','第一章 基础语法','change','已修改 course 。','14','1'), ('124','2017-11-29 11:56:48.114879','127.0.0.1','10','python高级编程','change','已修改 teacher，tag，youneed_know 和 teacher_tell 。','13','1'), ('125','2017-11-29 12:18:18.011114','127.0.0.1','4','无服务器架构系列讲解','change','已修改 name 。','13','1'), ('126','2017-11-29 12:18:39.682219','127.0.0.1','6','AWS云中的设计高可用架构','change','已修改 name 。','13','1'), ('127','2017-11-29 12:19:22.710501','127.0.0.1','6','AWS云设计高可用架构','change','已修改 name 。','13','1');
INSERT INTO `xadmin_usersettings` VALUES ('1','dashboard:home:pos','2','1'), ('2','site-theme','https://bootswatch.com/3/sandstone/bootstrap.min.css','1'), ('3','dashboard:home:pos','','5'), ('4','users_userprofile_editform_portal','box-0,box-1,box-2,box-3,box-4|box-5','1');
INSERT INTO `xadmin_userwidget` VALUES ('2','home','list','{\"model\": \"users.userprofile\", \"title\": \"first\"}','1');
