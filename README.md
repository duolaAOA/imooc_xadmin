# imooc_xadmin
django+xadmin在线教育平台

## environment 
* python==3.5.2
* django==1.10
* xadmin==0.6
* django-crispy-forms (1.6.1)
* django-formtools (2.0)
* django-import-export (0.5.1)
* django-pure-pagination (0.3.0)
* django-simple-captcha (0.4.6)
* future (0.16.0)
* httplib2 (0.10.3)
* Pillow (4.2.1)
* six (1.10.0)
* 视频存储用的[七牛云](https://www.qiniu.com/)(有免费的固定空间)


## The chart
![](https://github.com/duolaAOA/imooc_xadmin/blob/master/img.png?raw=true)


## Run screenshots
![](https://github.com/duolaAOA/imooc_xadmin/blob/master/imooc.gif?raw=true)
